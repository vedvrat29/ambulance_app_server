const db = require('../../core/db');

module.exports = {
    create: (contentInfo) => {
        return db('Hospital').insert(contentInfo).returning('*');
    },
    getHospital: (data) => {
        let query = db('Hospital as h')
            .select('h.*', 'Users.phoneNumber', 'Users.email')
            .innerJoin('Users', 'h.userId', 'Users.id')
        if (data) {
            if (data.id) {
                query = query.where({ 'h.id': data.id });
            } if (data.type) {
                query = query.where({ 'h.type': data.type });
            } if (data.userId) {
                query = query.where({ 'h.userId': data.userId });
            } if ((data.isActive == true) || (data.isActive == false)) {
                query = query.where({'h.isActive': data.isActive});
            }
        }
        // console.log(query);
        return query.returning('*');
    },
    getHospitalAllData: (data) => {
        let query = db('Hospital as h')
            // .select('h.*', 'Users.phoneNumber', 'Users.email')

            .select('Driver.id as driverId', 'Driver.hospitalId', 'Driver.ambulanceId', 'Driver.licenseNumber', 'Driver.aadharCardNumber', 'Driver.documentDetails', 'Driver.isActive',
                'Users.email as hospitalemail', 'Users.phoneNumber as hospitalNumber',
                'Ambulance.type as ambulanceType', 'Ambulance.numberPlate', 'Ambulance.status as ambulanceStatus',
                'h.name as hospitalName','h.type as hospitalType', 'h.latitude', 'h.longitude','h.addressLine1','h.addressLine2','h.landmark','h.city','h.state')
            .innerJoin('Users', 'h.userId', 'Users.id')
            .innerJoin('Ambulance', 'h.id', 'Ambulance.hospitalId')
            .innerJoin('Driver', 'h.id', 'Driver.hospitalId')
            .leftJoin('UserValidation', 'h.userId', 'UserValidation.userId')
            
        if (data) {
            if (data.id) {
                query = query.where({ 'h.id': data.id });
            } if (data.hospitalType) {
                query = query.where({ 'h.type': data.hospitalType });
            }if (data.ambulanceType) {
                query = query.where({ 'Ambulance.type': data.ambulanceType });
            } if (data.userId) {
                query = query.where({ 'h.userId': data.userId });
            } if ((data.isActive == true) || (data.isActive == false)) {
                query = query.where({'h.isActive': data.isActive});
            }
        }
        // console.log(query);
        return query.returning('*');
    },
    getHospitalList: (data) => {
        let query = db('Hospital as h')
        .select('Users.email as hospitalemail', 'Users.phoneNumber as hospitalNumber',
        'Ambulance.type as ambulanceType',
        'h.name as hospitalName','h.id as hospitalId','h.type as hospitalType', 'h.latitude', 'h.longitude','h.addressLine1','h.addressLine2','h.landmark','h.city','h.state')
        .innerJoin('Users', 'h.userId', 'Users.id')
        .innerJoin('Ambulance', 'h.id', 'Ambulance.hospitalId')
        .distinct('h.name')
    if (data) {
        if (data.id) {
            query = query.where({ 'h.id': data.id });
        } if (data.hospitalType) {
            query = query.where({ 'h.type': data.hospitalType });
        }if (data.ambulanceType) {
            query = query.where({ 'Ambulance.type': data.ambulanceType });
        } 
        // if (data.userId) {
        //     query = query.where({ 'h.userId': data.userId });
        // } if ((data.isActive == true) || (data.isActive == false)) {
        //     query = query.where({'h.isActive': data.isActive});
        // }
    }
    // console.log(query);
    return query.returning('*');
    },
   
    // getAllHospital: () => {
    //     return db('Hospital as h')
    //         .select('h.*', 'Users.phoneNumber', 'Users.email')
    //         .innerJoin('Users', 'h.userId', 'Users.id')
    //         .where('h.isActive', [true]);
    // },
    updateHospitalByUserId: (data, userId) => {
        return db('Hospital').where({ userId: userId }).update(data).returning('*');
    },
    updateHospitalById: (data, id) => {
        return db('Hospital').where({ id: id }).update(data).returning('*');
    },

    deleteHospitalById: (id) => {
        return db('Hospital').where({ id }).del().returning('id');
    },
    getHospitalCount: (type) => {
        return db('Hospital').where({ type: type }).whereIn('isActive', [true]).count('*').first()
    },
    getAllHospitalCount: () => {
        return db('Hospital').count('*').returning('*');
    },
    getGovernmentHospitalCount: () => {
        return db('Hospital').whereIn('type', ['GOVERNMENT']).count('*').returning('*');
    },
    getPrivateHospitalCount: () => {
        return db('Hospital').whereIn('type', ['PRIVATE']).count('*').returning('*');
    },

};
