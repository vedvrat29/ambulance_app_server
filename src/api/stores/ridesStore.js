const db = require('../../core/db');

module.exports = {
    create: (contentInfo) => {
        return db('Rides').insert(contentInfo).returning('*');
    },
    getRides: (data) => {
        let query = db('Rides')

        .select('Rides.id as rideId', 'Rides.userId as customerId','Rides.driverId', 'Rides.ambulanceId', 'Rides.hospitalId', 'Rides.bookingId as BookingId',
        'Users.fullName as patientName','Users.phoneNumber as PatientPhoneNumber','Hospital.name as hospitalName','Ambulance.numberPlate as ambulanceNumberPlate','Ambulance.type as ambulanceType','Rides.pickupPoint', 'Rides.dropoffPoint', 'Rides.status as rideStatus', 'Rides.ridesSummary', 'Rides.locationSummary','Rides.createdAt')
            .leftJoin('Users', 'Rides.userId', 'Users.id')
            .leftJoin('Ambulance', 'Rides.ambulanceId', 'Ambulance.id')
            .leftJoin('Hospital', 'Rides.hospitalId', 'Hospital.id')
        if (data) {
            if (data.id) {
                query = query.where({ 'Rides.id': data.id });
            } if (data.userId) {
                query = query.where({ 'Rides.userId': data.userId });
            } if (data.ambulanceId) {
                query = query.where({ 'Rides.ambulanceId': data.ambulanceId });
            } if (data.hospitalId) {
                query = query.where({ 'Rides.hospitalId': data.hospitalId });
            } if (data.driverId) {
                query = query.where({ 'Rides.driverId': data.driverId });
            } if (data.rideStatus) {
                query = query.where({ 'Rides.status': data.rideStatus });
            }
        }
        return query.returning('*');
    },
    getAllRides: () => {
        return db('Rides').returning('*');
    },
    updateRidesById: (data, id) => {
        return db('Rides').where({ id: id }).update(data).returning('*');
    },
    deleteRidesById: (id) => {
        return db('Rides').where({ id }).del().returning('id');
    },
    getAllRidesCount: () => {
        return db('Rides').count('*').returning('*');
    },
    getAllAmbulanceRidesCount: (hospitalId) => {
        return db('Rides').where({ 'Rides.hospitalId': hospitalId }).count('*');
    },
    getAllHospitalRidesData: (hospitalId) => {
        return db('Rides').where({ 'Rides.hospitalId': hospitalId }).returning('*');
    },
    getAllHospitalRidesCount: (hospitalId) => {
        return db('Rides').where({ 'Rides.hospitalId': hospitalId }).count('*');
    },
    getRidesByAmbulanceType: () => {
        return db('Rides')
            .innerJoin('Ambulance', 'Ambulance.id', 'Rides.ambulanceId')
            .select(['Ambulance.type', db.raw('CAST("Rides"."ambulanceId" AS UUID) AS "ambulanceId"')]);
    },








};
