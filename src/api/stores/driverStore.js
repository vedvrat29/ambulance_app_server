const db = require('../../core/db');

module.exports = {
    create: (contentInfo) => {
        return db('Driver').insert(contentInfo).returning('*');
    },

    getAllDriver: (data) => {
        // return db('Driver')
        let query = db('Driver')
            .select('Driver.id as driverId', 'Driver.userId as driverUserId', 'Driver.hospitalId', 'Driver.ambulanceId', 'Driver.licenseNumber', 'Driver.aadharCardNumber', 'Driver.documentDetails', 'Driver.isActive',
                'Users.email', 'Users.phoneNumber', 'Users.fullName', 'Users.dob', 'Users.gender', 'Users.approved',
                'Ambulance.type', 'Ambulance.numberPlate', 'Ambulance.status as ambulanceStatus',
                'Address.addressLine1', 'Address.addressLine2', 'Address.landmark', 'Address.city', 'Address.state', 'Address.postalCode', 'Hospital.name as hospitalName', 'Hospital.latitude', 'Hospital.longitude')
            .leftJoin('Users', 'Driver.userId', 'Users.id')
            .leftJoin('Ambulance', 'Driver.ambulanceId', 'Ambulance.id')
            .leftJoin('Hospital', 'Driver.hospitalId', 'Hospital.id')
            .leftJoin('Address', 'Users.id', 'Address.userId')
            if (data) {
               if ((data.isActive == true) || (data.isActive == false)) {
                query = query.where({'Driver.isActive': data.isActive});
            }
            }
            return query.returning('*');
    },
    getAllOnlineDrivers: (data) => {
        let query = db('Driver')
            .select('Driver.id as driverId', 'Driver.userId as driverUserId', 'Driver.hospitalId', 'Driver.ambulanceId', 'Driver.licenseNumber', 'Driver.aadharCardNumber', 'Driver.documentDetails', 'Driver.isActive',
                'Users.email', 'Users.phoneNumber', 'Users.fullName', 'Users.dob', 'Users.gender', 'Users.approved',
                'Address.addressLine1', 'Address.addressLine2', 'Address.landmark', 'Address.city', 'Address.state', 'Address.postalCode', 'Hospital.name as hospitalName', 'Hospital.type as hospitalType','Ambulance.type as ambulanceType', 'Hospital.latitude', 'Hospital.longitude')
            .leftJoin('Users', 'Driver.userId', 'Users.id')
            .leftJoin('Ambulance', 'Driver.ambulanceId', 'Ambulance.id')
            .leftJoin('Hospital', 'Driver.hospitalId', 'Hospital.id')
            .leftJoin('Address', 'Users.id', 'Address.userId')
            .leftJoin('UserValidation', 'Driver.userId', 'UserValidation.userId')
            .where('UserValidation.isCurrentSession', true)
            if (data) {
                if (data.id) {
                    query = query.where({ 'Ambulance.type': data.type });
                }  if (data.ambulanceType) {
                    query = query.where({'Ambulance.type': data.ambulanceType});
                }
            }
            return query.returning('*');
    },
    getDriverWithUserById: (driverId) => {
        return db('Driver')
            .select('Driver.id as driverId', 'Driver.userId as driverUserId', 'Driver.hospitalId', 'Driver.ambulanceId', 'Driver.licenseNumber', 'Driver.aadharCardNumber', 'Driver.documentDetails', 'Driver.isActive',
                'Users.email', 'Users.phoneNumber', 'Users.fullName', 'Users.dob', 'Users.gender', 'Users.approved',
                'Ambulance.type', 'Ambulance.numberPlate', 'Ambulance.status as ambulanceStatus',
                'Address.addressLine1', 'Address.addressLine2', 'Address.landmark', 'Address.city', 'Address.state', 'Address.postalCode', 'Hospital.name as hospitalName')
            .leftJoin('Users', 'Driver.userId', 'Users.id')
            .leftJoin('Ambulance', 'Driver.ambulanceId', 'Ambulance.id')
            .leftJoin('Hospital', 'Driver.hospitalId', 'Hospital.id')
            .leftJoin('Address', 'Users.id', 'Address.userId')
            .where('Driver.userId', driverId)
    },
    getDriver: (data) => {
        let query = db('Driver')
        .select('Driver.id as driverId', 'Driver.userId as driverUserId', 'Driver.hospitalId', 'Driver.ambulanceId', 'Driver.licenseNumber', 'Driver.aadharCardNumber', 'Driver.documentDetails', 'Driver.isActive',
                'Users.email', 'Users.phoneNumber', 'Users.fullName', 'Users.fcmToken','Users.dob', 'Users.gender', 'Users.approved',
                'Ambulance.type', 'Ambulance.numberPlate', 'Ambulance.status as ambulanceStatus',
                'Address.addressLine1', 'Address.addressLine2', 'Address.landmark', 'Address.city', 'Address.state', 'Address.postalCode', 'Hospital.name as hospitalName','Hospital.doctorName','Hospital.doctorPhoneNumber')
            .leftJoin('Users', 'Driver.userId', 'Users.id')
            .leftJoin('Ambulance', 'Driver.ambulanceId', 'Ambulance.id')
            .leftJoin('Hospital', 'Driver.hospitalId', 'Hospital.id')
            .leftJoin('Address', 'Users.id', 'Address.userId')
        if (data) {
            if (data.id) {
                query = query.where({ 'Driver.id': data.id });
            }
            if (data.userId) {
                query = query.where({ 'Driver.userId': data.userId });
            } 
            if ((data.isActive == true) || (data.isActive == false)) {
                 query = query.where({'Driver.isActive': data.isActive});
            } if (data.hospitalId) {
                query = query.where({'Driver.hospitalId': data.hospitalId});
            } if (data.ambulanceType) {
                query = query.where({'Ambulance.type': data.ambulanceType});
            }
        }
        return query.returning('*');
    },
    getDriverDetailsForDetails: (data) => {
        let query = db('Driver')
        .select('Driver.id as driverId', 'Driver.userId as driverUserId', 'Driver.hospitalId', 'Driver.ambulanceId', 'Driver.licenseNumber', 'Driver.aadharCardNumber', 'Driver.documentDetails', 'Driver.isActive',
                'Users.email', 'Users.phoneNumber', 'Users.fullName', 'Users.fcmToken','Users.dob', 'Users.gender', 'Users.approved',
                'Ambulance.type', 'Ambulance.numberPlate', 'Ambulance.status as ambulanceStatus',
                'Address.addressLine1', 'Address.addressLine2', 'Address.landmark', 'Address.city', 'Address.state', 'Address.postalCode', 'Hospital.name as hospitalName')
            .leftJoin('Users', 'Driver.userId', 'Users.id')
            .leftJoin('Ambulance', 'Driver.ambulanceId', 'Ambulance.id')
            .leftJoin('Hospital', 'Driver.hospitalId', 'Hospital.id')
            .leftJoin('Address', 'Users.id', 'Address.userId')
        if (data) {
            if (data.id) {
                query = query.where({ 'Driver.id': data.id });
            }
            if (data.userId) {
                query = query.where({ 'Driver.userId': data.userId });
            } 
            if ((data.isActive == true) || (data.isActive == false)) {
                 query = query.where({'Driver.isActive': data.isActive});
            } if ((data.hospitalId)) {
                query = query.where({'Driver.hospitalId': data.hospitalId});
            }
        }
        return query.returning('*');
    },
    updateDriverById: (data, id) => {
        return db('Driver').where({ id: id }).update(data).returning('*');
    },
    updateDriverByUserId: (data, userId) => {
        return db('Driver').where({ userId: userId }).update(data).returning('*');
    },
    deleteDriverById: (id) => {
        return db('Driver').where({ id }).del().returning('id');
    },
    getAllDriverCount: () => {
        return db('Driver').whereIn('isActive', [true]).count('*').returning('*');
    },
    getAllHospitalDriverCount: (hospitalId) => {
        return db('Driver').where({ 'Driver.hospitalId': hospitalId }).whereIn('isActive', [true]).count('*');
    },
    getAllHospitalDriverData: (hospitalId) => {
        return db('Driver').where({ 'Driver.hospitalId': hospitalId }).whereIn('isActive', [true]).returning('*');
    },
    getDriverbyAmbulanceIdData: (ambulanceId) => {
        return db('Driver').where({ 'Driver.ambulanceId': ambulanceId }).whereIn('isActive', [true]).returning('*');
    },
};
