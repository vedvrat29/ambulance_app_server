const db = require('../../core/db');

module.exports = {
    create: (contentInfo) => {
        return db('Users').insert(contentInfo).returning('*');
    },
    getusers: (data) => {
        let query = db('Users');
        
        if (data) {
            if (data.id) {
                query = query.where({ 'id': data.id });
            }
            if (data.scope) {
                query = query.whereRaw(`scope::text LIKE ?`, `%${data.scope}%`)
              }
        }
        return query.returning('*');
    },
    getAllusers: () => {
        return db('Users').returning('*');
    },
    updateusersById: (data, id) => {
        return db('Users').where({ id: id }).update(data).returning('*');
    },
    deleteusersById: (id) => {
        return db('Users').where({ id }).del().returning('id');
    },
    getByUserId: (id) => {return db('Users').where({ id }).first()
    },
    getByUserEmail: (email) => {return db('Users').where({ email }).first()
    },
    getUserByPhoneNumber: (phoneNumber) => {return db('Users').where({ phoneNumber }).first()
    },
    updateUserToken: (id, authToken) => {
        return db('Users').where({ id }).update({ authToken }).returning('*')
    },
};
