const db = require('../../core/db');

module.exports = {
    create: (contentInfo) => {
        return db('Booking').insert(contentInfo).returning('*')
    },
    getBooking: (data) => {
        let query = db('Booking')
        if (data) {
            if (data.id) {
                query = query.where({ 'Booking.id': data.id })
            }
            if (data.bookingId) {
                query = query.where({ 'Booking.bookingId': data.bookingId })
            }
            if (data.userId) {
                query = query.where({ 'Booking.userId': data.userId })
            }
        }
        return query.returning('*')
    },
    getAllBooking: () => {
        return db('Booking').returning('*')
    },
    updateBookingById: (data, id) => {
        return db('Booking').where({ id: id }).update(data).returning('*')
    },
    deleteBookingById: (id) => {
        return db('Booking').where({ id }).del().returning('id');
    },
    updateBookingSatatusByBookingID: (data, bookingId) => {
        return db('Booking').where({ bookingId }).update(data).returning('*')
    },
}