const db = require('../../core/db');

module.exports = {
  save: (info) => {
    return db('UserValidation').insert(info).returning('*')
  },
  getByUserId: (userId) => {
    return db('UserValidation').select('*').where({ userId }).first()
  },
  createUserValidation: ({ userId, createdBy, emailValidationKey }) => {
    return db('UserValidation').insert({ userId, createdBy, emailValidationKey }).returning('id')
  },
  createCustomerValidation: ({ customerId, createdBy, emailValidationKey }) => {
    return db('CustomerValidation').insert({ customerId, createdBy, emailValidationKey }).returning('id')
  },
  createVendorValidation: ({ userId, createdBy, isEmailValid, isPhoneValid, isAcceptTerms, isAcceptPrivacy }) => {
    return db('UserValidation').insert({ userId, createdBy, isEmailValid, isPhoneValid, isAcceptTerms, isAcceptPrivacy }).returning('id')
  },
  findValidationKey: (data) => {
    return db('UserValidation').where(data).select('*')
  },
  emailVerifiedToken: (emailValidationKey, data) => {
    return db('UserValidation').where(emailValidationKey).update(data).returning('id')
  },
  getUserValidations: (id) => {
    return db('UserValidation').where({ userId: id }).select('*').first()
  },
  update: (userId, info) => {
    return db('UserValidation').where({ userId }).update(info).returning('*')
  },
  getScopeByUserId: (id) => {
    return db('Users').select('firstName', 'lastName', 'email','address', 'gstNumber', 'username', 'scope').where({ id }).first()
  },
  delete: (data) => {
    return db('UserValidation').where(data).del()
  },
  getUserBykey: (data) => {
    return db('UserValidation')
      .where(data)
      .innerJoin('Users', 'Users.id', 'UserValidation.userId')
      .select('Users.id', 'Users.fullName', 'Users.approved', 'Users.email', 'Users.phoneNumber','Users.address', 'Users.gstNumber',
        'UserValidation.isDocSubmitted', 'UserValidation.isPhoneValid',
        'UserValidation.isEmailValid')
  },

  // customer
  deleteCustomerByUserId: (userId) => {
    return db('UserValidation').where({ userId }).del();
  },


  // Admin queries
  getPendingCustomers: () => {
    return db('UserValidation')
      .innerJoin('Users', 'Users.id', 'UserValidation.userId')
      .where('Users.approved', false)
      .andWhere('UserValidation.isDocSubmitted', true)
      .select('Users.id', 'Users.fullName', 'Users.approved', 'Users.email', 'Users.phoneNumber','Users.address', 'Users.gstNumber',
        'UserValidation.isDocSubmitted', 'UserValidation.isPhoneValid',
        'UserValidation.isEmailValid')
  },

  getPendingCustomersByServerValidation: (sorting, pagination) => {

    let limit = pagination.limit
    let offset = pagination.offset
    // sorting
    let sortBy = sorting.sortBy
    let sortOrder = sorting.sortOrder


    let query = db('UserValidation')
      .innerJoin('Users', 'Users.id', 'UserValidation.userId')
      .where('Users.approved', false)
      .andWhere('UserValidation.isDocSubmitted', true)
      .select('Users.id', 'Users.fullName', 'Users.approved', 'Users.email', 'Users.phoneNumber','Users.address', 'Users.gstNumber',
        'UserValidation.isDocSubmitted', 'UserValidation.isPhoneValid',
        'UserValidation.isEmailValid')

    if (sortBy) {
      sortOrder = sortOrder === undefined ? 'asc' : sortOrder
      query = query.orderBy(`UserValidation.${sortBy}`, sortOrder)
    }
    // pagination
    if (limit > 0 && offset >= 0) {
      query = query.limit(+limit).offset(+offset)
    }
    return query


  }
};
