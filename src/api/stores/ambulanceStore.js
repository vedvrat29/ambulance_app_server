const db = require('../../core/db');

module.exports = {
    create: (contentInfo) => {
        return db('Ambulance').insert(contentInfo).returning('*');
    },
    getAmbulances: (data) => {
        let query = db('Ambulance as a')
            .select('a.*', 'Hospital.name as hospitalName')
            .innerJoin('Hospital', 'a.hospitalId', 'Hospital.id')
        if (data) {
            if (data.id) {
                query = query.where({ 'a.id': data.id });
            }
            if (data.hospitalId) {
                query = query.where({ 'a.hospitalId': data.hospitalId })
            }
            if (data.type) {
                query = query.where({ 'a.type': data.type })
            }
            if (data.numberPlate) {
                query = query.where({ 'a.numberPlate': data.numberPlate })
            }
            if ((data.isActive == true) || (data.isActive == false)) {
                query = query.where({'Driver.isActive': data.isActive});
            }
            
        }
        return query.returning('*');
    },
    updateAmbulanceById: (data, id) => {
        return db('Ambulance').where({ id: id }).update(data).returning('*');
    },
    deleteAmbulanceById: (id) => {
        return db('Ambulance').where({ id }).del().returning('id');
    },
    getAmbulanceCountByType: async (type) => {
        let query = db('Ambulance').count('*');

        if (type) {
            query = query.where('type', type).whereIn('isActive', [true]);
        }

        const result = await query.first();

        return result.count('*').returning('*');
    },
    getAllAmbulancesData: () => {
        return db('Ambulance as a').select('a.*', 'Hospital.name as hospitalName').innerJoin('Hospital', 'a.hospitalId', 'Hospital.id')
            .whereIn('a.isActive', [true]);
    },

    getAllHospitalAmbulanceCount: (hospitalId) => {
        return db('Ambulance').where({ 'Ambulance.hospitalId': hospitalId }).whereIn('isActive', [true]).count('*');
    },

    getAllAmbulanceCount: () => {
        return db('Ambulance').count('*').returning('*');
    },
    getBasicAmbulanceCount: () => {
        return db('Ambulance').whereIn('type', ['BASIC']).count('*').returning('*');
    },
    getAdvanceAmbulanceCount: () => {
        return db('Ambulance').whereIn('type', ['ADVANCE']).count('*').returning('*');
    },
    getMaternityAmbulanceCount: () => {
        return db('Ambulance').whereIn('type', ['MATERNITY']).count('*').returning('*');
    },

    getAllHospitalAmbulanceData: (hospitalId) => {
        return db('Ambulance').where({ 'Ambulance.hospitalId': hospitalId }).whereIn('isActive', [true]).returning('*');
    },
    getAmbulanceByNumberPlate: (numberPlate) => {
        return db('Ambulance').where({ 'Ambulance.numberPlate': numberPlate }).whereIn('isActive', [true]).returning('*');
    },
    getAmbulanceById: (id) => {
        return db('Ambulance').where({ 'Ambulance.id': id }).whereIn('isActive', [true]).returning('*');
    },
};
