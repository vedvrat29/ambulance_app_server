const Joi = require('joi');
const { signupHospital } = require('../../core/auth/signup');

const isEmpty = require('../../core/utils/isEmpty');
const hospitalStore = require('../stores/hospitalStore');
const usersStore = require('../stores/usersStore');
const usersValidationStore = require('../stores/usersValidationStore');
const ambulanceStore = require('../stores/ambulanceStore');
const driverStore = require('../stores/driverStore');
const axios = require('axios');

const dotenv = require('dotenv'); // Load environment variables
dotenv.config();

const { GOOGLE_MAP_API } = process.env;

module.exports = [
    //get All Hospital
    {
        method: 'GET',
        path: '/hospital',
        options: {
            tags: ['api'],
            auth: false,
            description: 'Get Hospital',
            notes: 'Returns a list of Hospital',
        },
        handler: async (request, h) => {
            try {
                const getHospitalList = await hospitalStore.getHospital();
                return h.response({
                    statusCode: 200,
                    status: true,
                    message: `List of all Hospital`,
                    data: getHospitalList
                });

            } catch (error) {
                console.error('Database error:', error);
                return handler.response({
                    statusCode: 500,
                    status: false,
                    error: 'Internal Server Error',
                    message: 'An error occurred while get all Hospital list.',
                    data: null
                })
            }
        },
    },
    //Add Hospital
    {
        method: 'POST',
        path: '/hospital/add',
        config: {
            // auth: 'jwt', // Apply JWT authentication to this route
            auth: false,
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    name: Joi.string().required().description('Hospital name'),
                    type: Joi.string().valid('GOVERNMENT', 'PRIVATE').required().description('Ambulance type'),
                    phoneNumber: Joi.string().required().description('Ambulance Phone Number'),
                    email: Joi.string().required().description('Ambulance email'),
                    addressLine1: Joi.string().required().description('Ambulance type'),
                    addressLine2: Joi.string().optional().description('Ambulance type'),
                    landmark: Joi.string().optional().description('Hospital landmark'),
                    city: Joi.string().description('Hospital landmark'),
                    state: Joi.string().description('Hospital landmark'),
                    postalCode: Joi.string().description('Hospital landmark'),
                    latitude: Joi.number().optional().description('Hospital latitude'),
                    longitude: Joi.number().optional().description('Hospital longitude'),
                    password: Joi.string().optional().description('Hospital Password'),
                    signupType: Joi.string().optional().description('signup type for Hospital'),
                }),
            },
            description: 'Admin add Hospital for the website'
        },
        handler: async (request, handler) => {

            let createdBy = 'ADMIN';
            let userRole = 'HOSPITAL';
            let email = request.payload.email.toLowerCase();
            let { name, type, phoneNumber, password, signupType, addressLine1, addressLine2, landmark, city, state, postalCode, latitude, longitude } = request.payload
            try {

                let userEmailExists = await usersStore.getByUserEmail(email)
                let phoneNumberExists = await usersStore.getUserByPhoneNumber(request.payload.phoneNumber)

                if (userEmailExists) {
                    return handler.response({
                        statusCode: 400,
                        status: false,
                        error: "Failed",
                        message: `Email is already exist`,
                        data: null
                    })
                }

                if (phoneNumberExists) {
                    return handler.response({
                        statusCode: 400,
                        status: false,
                        error: 'Failed',
                        message: `Phone number is already exist`,
                        data: null
                    })
                }
                // signup driver at users table and with scope and role
                let signupHospitalPayload = { fullName: name, email: email, password: password, phoneNumber: phoneNumber, signupType: signupType, createdBy: createdBy }
                let { authToken, user } = await signupHospital(signupHospitalPayload, userRole)
                // Add restof information into Hospital table
                let hospitalPayload = {
                    userId: user.data.id,
                    name,
                    type,
                    addressLine1,
                    addressLine2,
                    landmark,
                    city,
                    state,
                    postalCode,
                    latitude,
                    longitude,
                    isActive: true,
                    createdBy
                }
                let hospitalDetails = await hospitalStore.create(hospitalPayload);
                // Add driver details into uservalidations
                // Genearte Token for email verification
                let token = "1234567fadfhjknfmdsa";

                await usersValidationStore.createUserValidation({ userId: user.data.id, createdBy: email, emailValidationKey: token, active: true, createdBy: createdBy })

                // console.log("savedContent", savedContent);
                return ({
                    statusCode: 200,
                    status: true,
                    error: ``,
                    message: 'New hospital details has been successfully added',
                    data: hospitalDetails
                })
            }
            catch (e) {
                console.error('Error adding Hospital:', e);
                return ({
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while adding the Hospital.',
                    data: null
                })
            }
        }
    },
    // Edit Hospital
    {
        method: 'PATCH',
        path: '/hospital/edit',
        config: {
            // auth: 'jwt', // Apply JWT authentication to this route
            auth: false,
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    hospitalUserId: Joi.string().required().description('ID of the Hospital to be edited'),
                    name: Joi.string().required().description('Hospital name'),
                    type: Joi.string().valid('GOVERNMENT', 'PRIVATE').required().description('Ambulance type'),
                    phoneNumber: Joi.string().required().description('Ambulance Phone Number'),
                    email: Joi.string().required().description('Ambulance email'),
                    addressLine1: Joi.string().required().description('Ambulance type'),
                    addressLine2: Joi.string().optional().description('Ambulance type'),
                    landmark: Joi.string().optional().description('Hospital landmark'),
                    city: Joi.string().description('Hospital landmark'),
                    state: Joi.string().description('Hospital landmark'),
                    postalCode: Joi.string().description('Hospital landmark'),
                    latitude: Joi.number().optional().description('Hospital latitude'),
                    longitude: Joi.number().optional().description('Hospital longitude'),
                    password: Joi.string().optional().description('Hospital Password'),
                    signupType: Joi.string().optional().description('signup type for Hospital'),
                    isActive: Joi.boolean().optional().description('is active hospital status'),
                }),
            },
            description: 'Admin Edit Hospital for the website'
        },
        handler: async (request, handler) => {
            let { hospitalUserId, name, type, phoneNumber, email, password, signupType, addressLine1, addressLine2, landmark, city, state, postalCode, latitude, longitude, isActive } = request.payload
            const modifiedAt = new Date().toISOString();
            const modifiedBy = 'ADMIN';

            try {

                //get hospitalDetails by hostpitalid
                let data = {
                    userId: hospitalUserId
                }
                const hospitalDetails = await hospitalStore.getHospital(data);
                // console.log("hospitalDetails", hospitalDetails);

                //get userDetails by userId
                const userDetails = await usersStore.getByUserId(hospitalUserId);
                // console.log("userDetails", userDetails);

                //checking email and phone number exist or not
                let userEmailExists = await usersStore.getByUserEmail(email)
                let phoneNumberExists = await usersStore.getUserByPhoneNumber(request.payload.phoneNumber)
                if (userDetails.email != request.payload.email) {
                    // console.log("email match");

                    if (userEmailExists) {
                        return handler.response({
                            statusCode: 400,
                            status: false,
                            error: "Failed",
                            message: `Email is already exist`,
                            data: null
                        })
                    }
                }
                if (userDetails.phoneNumber != request.payload.phoneNumber) {
                    // console.log("phoneNumber match");

                    if (phoneNumberExists) {
                        return handler.response({
                            statusCode: 400,
                            status: false,
                            error: 'Failed',
                            message: `Phone number is already exist`,
                            data: null
                        })
                    }
                }
                // console.log("Not email match & Not phone number match");

                // Check if the ride exists
                const existingRide = await hospitalStore.getHospital(hospitalUserId);
                if (!existingRide) {
                    return {
                        status: false,
                        statusCode: 404,
                        error: 'Not Found',
                        message: 'Ride not found',
                        data: null
                    };
                }

                // Perform the update
                const updatedHospital = await hospitalStore.updateHospitalByUserId({
                    name, type, addressLine1, addressLine2, landmark, city, state, postalCode, latitude, longitude, modifiedAt, modifiedBy, isActive
                }, hospitalUserId);
                // console.log("updatedHospital", updatedHospital);

                //perfron the user table update
                const updateUser = await usersStore.updateusersById({
                    phoneNumber: request.payload.phoneNumber, email: request.payload.email, password: request.payload.password, fullName: request.payload.name
                }, hospitalUserId)
                // console.log("updateUser", updateUser);

                // Return success response
                return {
                    statusCode: 200,
                    status: true,
                    error: '',
                    message: 'Hospital details has been successfully updated',
                    data: updatedHospital
                };
            } catch (e) {
                // console.error('Error updating Hospital:', e);
                return {
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while updating the Hospital.',
                    data: null
                };
            }
        }
    },
    //Get By Id Hospital 
    {
        method: 'GET',
        path: '/hospital/{hospitalUserId}',
        options: {
            auth: false, // Apply JWT authentication to this route
            tags: ['api'],
            description: 'Get a Hospital by user Id',
            validate: {
                params: Joi.object({
                    hospitalUserId: Joi.string().required().description('ID of the Hospital user Id to fetch')
                })
            }
        },
        handler: async (request, h) => {
            const userId = request.params.hospitalUserId;
            try {
                const hospitalDetails = await hospitalStore.getHospital({ userId: userId });
                if (!isEmpty(hospitalDetails)) {
                    return h.response({
                        statusCode: 200,
                        status: true,
                        message: `Successfully fetched details by hospital userId ${userId}`,
                        data: hospitalDetails
                    });
                } else {
                    return h.response({
                        statusCode: 404,
                        status: false,
                        message: `Hospital with user ID ${userId} not found`,
                        data: null
                    });
                }
            } catch (error) {
                console.error(error);
                return {
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while fetching hospital data.',
                    data: null
                };
            }
        },
    },
    //Get By Hospital by Status 
    {
        method: 'GET',
        path: '/hospital/getByStatus/{isActive}',
        options: {
            auth: false, // Apply JWT authentication to this route
            tags: ['api'],
            description: 'Get a Hospital by isActive',
            validate: {
                params: Joi.object({
                    isActive: Joi.boolean().required().description('enter isActive status for hospital list')
                })
            }
        },
        handler: async (request, h) => {
            const isActive = request.params.isActive;
            try {
                const hospitalDetails = await hospitalStore.getHospital({ isActive: isActive });
                if (!isEmpty(hospitalDetails)) {
                    return h.response({
                        statusCode: 200,
                        status: true,
                        message: `Successfully fetched hospital details by isActive: ${isActive}`,
                        data: hospitalDetails
                    });
                } else {
                    return h.response({
                        statusCode: 404,
                        status: false,
                        message: `Hospital with isActive :${isActive} not found`,
                        data: null
                    });
                }
            } catch (error) {
                console.error(error);
                return {
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while fetching hospital data.',
                    data: null
                };
            }
        },
    },
    //get Hospital Count
    {
        method: 'GET',
        path: '/hospital/countByType/{type}',
        options: {
            auth: false, // Apply JWT authentication to this route
            tags: ['api'],
            description: 'Get a Hospital by ID',
            validate: {
                params: Joi.object({
                    type: Joi.string().required().description('ID of the Hospital to fetch')
                })
            }
        },
        handler: async (request, h) => {
            const type = request.params.type;
            try {
                const hospitalDetails = await hospitalStore.getHospitalCount(type);
                return {
                    statusCode: 200,
                    status: true,
                    message: `count of all hospital type ${type} Data`,
                    data: hospitalDetails
                };
            } catch (error) {
                return {
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while fetching hospital count data.',
                    data: null
                };
            }
        },
    },
    //patch - hospital checking Relations by id
    {
        method: 'PATCH',
        path: '/hospital/checking/relations/{hospitalUserId}',
        options: {
            auth: false, // Apply JWT authentication to this route
            tags: ['api'],
            description: 'checking Relations of Hospital by ID',
            validate: {
                params: Joi.object({
                    hospitalUserId: Joi.string().required().description('ID of the Hospital to delete')
                })
            }
        },
        handler: async (request, h) => {
            const hospitalUserId = request.params.hospitalUserId;
            let response, hospitalId;
            let savedContent = '';
            let existingAmbulanceData, existingDriverData;
            const modifiedAt = new Date().toISOString();
            const modifiedBy = 'ADMIN';

            try {

                let data = {
                    userId: hospitalUserId
                }

                savedContent = await hospitalStore.getHospital(data);
                // console.log("savedContent", savedContent);

                if (isEmpty(savedContent)) {
                    return h.response({
                        status: false,
                        statusCode: 404,
                        error: 'Not Find Hospital',
                        message: `Please provide valid Hospital Id ${hospitalUserId}`,
                        data: null
                    })
                }

                if (savedContent == '') {
                    return h.response({
                        status: false,
                        statusCode: 404,
                        error: 'Not Find Hospital',
                        message: `Please provide valid Hospital Id ${hospitalUserId}`,
                        data: null
                    })
                }


                //get hospita id
                hospitalId = savedContent[0].id

                const existingAmbulance = await ambulanceStore.getAllHospitalAmbulanceData(hospitalId);
                // console.log("existingAmbulance", existingAmbulance);
                if (isEmpty(existingAmbulance)) {
                    existingAmbulanceData = null;
                } else {
                    existingAmbulanceData = existingAmbulance;
                }

                const existingDriver = await driverStore.getAllHospitalDriverData(hospitalId);
                // console.log("existingDriver", existingDriver);
                if (isEmpty(existingDriver)) {
                    existingDriverData = null;
                } else {
                    existingDriverData = existingDriver;
                }

                response = {
                    hospitalData: savedContent[0],
                    existingAmbulanceData: existingAmbulanceData,
                    existingDriverData: existingDriverData
                }

                return {
                    statusCode: 200,
                    status: true,
                    message: `Relations of hospital`,
                    data: response
                };

            } catch (error) {
                return ({
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while Checking relations of Hospital.',
                    data: null
                })
            }
        },

    },
    //patch - hospital checking Relations by id
    {
        method: 'PATCH',
        path: '/hospital/remove/{hospitalUserId}',
        options: {
            auth: false, // Apply JWT authentication to this route
            tags: ['api'],
            description: 'checking Relations of Hospital by ID',
            validate: {
                params: Joi.object({
                    hospitalUserId: Joi.string().required().description('ID of the Hospital to delete')
                })
            }
        },
        handler: async (request, h) => {
            const hospitalUserId = request.params.hospitalUserId;
            let response, hospitalId;
            const modifiedAt = new Date().toISOString();
            const modifiedBy = 'ADMIN';

            try {
                let data = {
                    userId: hospitalUserId
                }
                const savedContent = await hospitalStore.getHospital(data);
                // console.log("savedContent", savedContent);
                hospitalId = savedContent[0].id

                const existingAmbulance = await ambulanceStore.getAllHospitalAmbulanceData(hospitalId);
                // console.log("existingAmbulance", existingAmbulance);
                const existingDriver = await driverStore.getAllHospitalDriverData(hospitalId);
                // console.log("existingDriver", existingDriver);

                if (existingAmbulance && existingDriver) {

                    // Perform the update
                    for (let i = 0; i < existingAmbulance.length; i++) {
                        const updatedAmbulance = await ambulanceStore.updateAmbulanceById({
                            isActive: false, modifiedAt, modifiedBy
                        }, existingAmbulance[i].id);
                    }

                    // Perform the update
                    for (let i = 0; i < existingDriver.length; i++) {
                        // Perform the update
                        const updatedDriver = await driverStore.updateDriverById({
                            isActive: false, modifiedAt, modifiedBy
                        }, existingDriver[i].id);
                        console.log("updatedDriver", updatedDriver);
                    }

                    // Perform the update
                    const updatedHospital = await hospitalStore.updateHospitalByUserId({
                        isActive: false, modifiedAt, modifiedBy
                    }, hospitalUserId);

                    return {
                        statusCode: 200,
                        status: true,
                        message: `Hospital Id ${hospitalUserId} Removed`,
                        data: response
                    };

                }

            } catch (error) {
                return ({
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while deleting the Hospital.',
                    data: null
                })
            }
        },

    },
    // Get Distances 
    {
        method: 'POST',
        path: '/getDistance/hospital',
        options: {
            auth: false, // Apply JWT authentication to this route
            tags: ['api'],
            description: 'Get all hospital distances',
            validate: {
                payload: Joi.object({
                    lat: Joi.number().default('18.5148893').required().description('Latitude:'),
                    lng: Joi.number().default('73.7823448').required().description('Longitude:'),
                    hospitalType: Joi.string().valid('PRIVATE','GOVERNMENT').required().description('Hospital Type'),
                    ambulanceType: Joi.string().valid('BASIC', 'ADVANCE', 'MATERNITY').required().description('Ambulance Type'),
                })
            }
        },
        handler: async (request, handler) => {
            try {
            const {lat,lng, hospitalType,ambulanceType} = request.payload;
            let hospitals = [];
            let hospitalDataByType = await hospitalStore.getHospitalList({hospitalType:hospitalType,ambulanceType:ambulanceType})
            // console.log(hospitalDataByType);
            for (let index = 0; index < hospitalDataByType.length; index++) {
                

                let hospitalPayload = {
                    name :hospitalDataByType[index].name,
                    driverId :hospitalDataByType[index].driverId,
                    hospitalId :hospitalDataByType[index].hospitalId,
                    ambulanceId :hospitalDataByType[index].ambulanceId,
                    licenseNumber :hospitalDataByType[index].licenseNumber,
                    aadharCardNumber :hospitalDataByType[index].aadharCardNumber,
                    documentDetails :hospitalDataByType[index].documentDetails,
                    isActive :hospitalDataByType[index].isActive,
                    hospitalemail :hospitalDataByType[index].hospitalemail,
                    hospitalNumber :hospitalDataByType[index].hospitalNumber,
                    ambulanceType :hospitalDataByType[index].ambulanceType,
                    numberPlate :hospitalDataByType[index].numberPlate,
                    ambulanceStatus :hospitalDataByType[index].ambulanceStatus,
                    hospitalName :hospitalDataByType[index].hospitalName,
                    hospitalType :hospitalDataByType[index].hospitalType,
                    latitude :hospitalDataByType[index].latitude,
                    longitude :hospitalDataByType[index].longitude,
                    addressLine1 :hospitalDataByType[index].addressLine1,
                    addressLine2 :hospitalDataByType[index].addressLine2,
                    city :hospitalDataByType[index].city,
                    state :hospitalDataByType[index].state,
                    lat: hospitalDataByType[index].latitude,
                    lng: hospitalDataByType[index].longitude,
                }   
                hospitals.push(hospitalPayload);
            }

            const apiKey = GOOGLE_MAP_API;

            const distancePromises = hospitals.map(async (hospital) => {
                const response = await axios.get(
                  `https://maps.googleapis.com/maps/api/distancematrix/json?origins=${lat},${lng}&destinations=${hospital.lat},${hospital.lng}&key=${apiKey}`
            );
                    
                if (
                    response.data &&
                    response.data.rows &&
                    response.data.rows.length > 0 &&
                    response.data.rows[0].elements &&
                    response.data.rows[0].elements.length > 0
                  ) {
                    const distance = response.data.rows[0].elements[0].distance.text;
                    const duration = response.data.rows[0].elements[0].duration.text;
                return {
                  hospitalName: hospital.name,
                  hospitalId: hospital.hospitalId,
                  hospitalemail :hospital.hospitalemail,
                  hospitalNumber :hospital.hospitalNumber,
                  ambulanceType :hospital.ambulanceType,
                  hospitalType :hospital.hospitalType,
                  latitude :hospital.latitude,
                  longitude :hospital.longitude,
                  addressLine1 :hospital.addressLine1,
                  addressLine2 :hospital.addressLine2,
                  city :hospital.city,
                  state :hospital.state,
                  distance: distance,
                  duration: duration,
                } 
                } else {
                    throw new Error(`Failed to calculate distance to ${hospital.name}`);
                  }
                });
      
              const distances = await Promise.all(distancePromises);
              console.log(`List of hospital distances`); 
                return handler.response({
                    status: true,
                    statusCode: 200,
                    error: '',
                    message: 'List of hospital distances',
                    data: distances || null
                })

            } catch (error) {
                console.error("Error fetching hospital data:", error);
                return {
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while fetching List of hospital distances',
                    data: null
                };
            }

        },
    },
];