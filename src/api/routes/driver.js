const Joi = require('joi');
const { Signup } = require('../../core/auth/signup');
const isEmpty = require('../../core/utils/isEmpty');
const driverStore = require('../stores/driverStore');
const userStore = require('../stores/usersStore');
const usersValidationStore = require('../stores/usersValidationStore');
const ambulanceStore = require('../stores/ambulanceStore');
const hospitalStore = require('../stores/hospitalStore');

module.exports = [
   
    //get All Driver
    {
        method: 'GET',
        path: '/driver',
        options: {
            tags: ['api'],
            auth: false,
            description: 'Get Driver',
            notes: 'Returns a list of Driver',
        },
        handler: async (request, h) => {
            try {
                const getDriverList = await driverStore.getAllDriver();
                // console.log("getDriverList",getDriverList);
                return h.response({
                    statusCode: 200,
                    status: true,
                    message: `List of all drivers`,
                    data: getDriverList
                });
            } catch (error) {
                console.error('Database error:', error);
                return handler.response({
                    statusCode: 500,
                    status: false,
                    error: 'Internal Server Error',
                    message: 'An error occurred while get all drivers list.',
                    data: null
                })
            }
        },
    },
     //Get driver by isActive
    {
        method: 'GET',
        path: '/driver/getByStatus/{isActive}',
        options: {
            auth: false, // Apply JWT authentication to this route
            tags: ['api'],
            description: 'Get a Driver by driver user Id',
            validate: {
                params: Joi.object({
                    isActive: Joi.boolean().required().description('get driver data by status true or false')
                })
            }
        },
        handler: async (request, h) => {
            const isActive = request.params.isActive;
            try {
                const driverDetails = await driverStore.getAllDriver({ isActive: isActive} );
                if (!isEmpty(driverDetails)) {
                    return h.response({
                        statusCode: 200,
                        status: true,
                        message: `Successfully fetched driver details by isActive: ${isActive}`,
                        data: driverDetails
                    });
                } else {
                    return h.response({
                        statusCode: 404,
                        status: false,
                        message: `Driver having isActive :${isActive} not found`,
                        data: null
                    });
                }
            } catch (error) {
                console.log(error);
                return h.response({
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while fetching Driver data.',
                    data: null
                });
            }
        },
    },
    //Get By Id Driver
    {
        method: 'GET',
        path: '/driver/{driverUserId}',
        options: {
            auth: false, // Apply JWT authentication to this route
            tags: ['api'],
            description: 'Get a Driver by driver user Id',
            validate: {
                params: Joi.object({
                    driverUserId: Joi.string().required().description('userId of the Driver to fetch')
                })
            }
        },
        handler: async (request, h) => {
            const driverUserId = request.params.driverUserId;
            try {
                const driverDetails = await driverStore.getDriverWithUserById(driverUserId);
                if (!isEmpty(driverDetails)) {
                    return h.response({
                        statusCode: 200,
                        status: true,
                        message: `driver ID ${driverUserId} Data`,
                        data: driverDetails
                    });
                } else {
                    return h.response({
                        statusCode: 404,
                        status: false,
                        message: `driver with ID ${driverUserId} not found`,
                        data: null
                    });
                }
            } catch (error) {
                console.log(error);
                return h.response({
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while fetching Driver data.',
                    data: null
                });
            }
        },
    },
    //Get Driver details via, ambulance type, Hospital Id
    {
        method: 'GET',
        path: '/driver/byHospitalId/{hospitalId}',
        options: {
            auth: false, // Apply JWT authentication to this route
            tags: ['api'],
            description: 'Get a Driver by driver user Id',
            validate: {
                params: Joi.object({
                    hospitalId: Joi.string().required().description('hospitalId to fetch all drivers')
                })
            }
        },
        handler: async (request, h) => {
            const hospitalId = request.params.hospitalId;
            try {
                const driverDetails = await driverStore.getDriver({hospitalId:hospitalId});
                if (!isEmpty(driverDetails)) {
                    return h.response({
                        statusCode: 200,
                        status: true,
                        message: `hospital Id ${hospitalId} Data`,
                        data: driverDetails
                    });
                } else {
                    return h.response({
                        statusCode: 404,
                        status: false,
                        message: `driver with ID ${hospitalId} not found`,
                        data: null
                    });
                }
            } catch (error) {
                console.log(error);
                return h.response({
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while fetching Driver data.',
                    data: null
                });
            }
        },
    },
  
    // Get all active/online drivers
    {
        method: 'GET',
        path: '/driver/online',
        options: {
            tags: ['api'],
            auth: false,
            description: 'Get all Online Drivers',
            notes: 'Returns a list of all online drivers',
        },
        handler: async (request, h) => {
            try {
                
                const getDriverList = await driverStore.getAllOnlineDrivers();
                // console.log(`in online driver apis`);
                return h.response({
                    statusCode: 200,
                    status: true,
                    message: `List of all online drivers`,
                    data: getDriverList
                });
            } catch (error) {
                console.log(error);
                return {
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while fetching drivers data.',
                    data: null
                };
            }
        },
    },
    //driver signup and add other data into driver table
    {
        method: 'POST',
        path: '/driver/add',
        config: {
            // auth: 'jwt', // Apply JWT authentication to this route
            auth: false,
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    hospitalId: Joi.string().optional().description('Driver associated Hospital'),
                    ambulanceId: Joi.string().optional().description('Driver associated Ambulance'),
                    fullName: Joi.string().optional().description('Driver fullname'),
                    email: Joi.string().required().description('Driver email'),
                    phoneNumber: Joi.string().required().description('Driver Phone Number'),
                    dob: Joi.string().optional().description('Driver dob'),
                    gender: Joi.string().optional().description('Driver gender'),
                    licenseNumber: Joi.string().optional().description('Driver license'),
                    aadharCardNumber: Joi.string().optional().description('Driver Aadharcard Number'),
                    password: Joi.string().optional().description('User Password'),
                    signupType: Joi.string().optional().description('User'),
                }),
            },
            description: 'Admin add Driver for the website'
        },
        handler: async (request, handler) => {

            let email = request.payload.email.toLowerCase();
            let { hospitalId, ambulanceId, fullName, phoneNumber, dob, gender, licenseNumber, aadharCardNumber, password, signupType } = request.payload
            let createdBy = 'Hospital';
            let userRole = 'DRIVER';
            try {

                let userEmailExists = await userStore.getByUserEmail(email)
                let phoneNumberExists = await userStore.getUserByPhoneNumber(request.payload.phoneNumber)

                if (userEmailExists) {
                    return handler.response({
                        statusCode: 400,
                        status: false,
                        error: "Signup Failed",
                        message: `Email is already exist`,
                        data: null
                    })
                }

                if (phoneNumberExists) {
                    return handler.response({
                        statusCode: 400,
                        status: false,
                        error: 'Signup Failed',
                        message: `Phone number is already exist`,
                        data: null
                    })
                }
                // signup driver at users table and with scope and role
                let signupPayload = { fullName: fullName, email: email, password: password, phoneNumber: phoneNumber, signupType: signupType, dob: dob, gender: gender, createdBy: createdBy }
                let { authToken, user } = await Signup(signupPayload, userRole)
                // Add restof information into driver table
                // Genearte Token for email verification
                let token = "1234567fadfhjknfmdsa";
                let driverDetails = await driverStore.create({ userId: user.data.id, hospitalId, ambulanceId, licenseNumber, isActive: true, aadharCardNumber, createdBy })
                // Add driver details into uservalidations
                await usersValidationStore.createUserValidation({ userId: user.data.id, createdBy: email, emailValidationKey: token, active: true, createdBy: createdBy })

                return handler.response({
                    statusCode: 200,
                    status: true,
                    error: ``,
                    message: 'New driver details has been successfully added',
                    data: driverDetails
                }).state('access_token', authToken)
            }
            catch (e) {
                console.error('Error adding Driver:', e);
                return handler.response({
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while adding the Driver.',
                    data: null
                })
            }
        }
    },
    // Edit Driver
    {
        method: 'PATCH',
        path: '/driver/edit',
        config: {
            // auth: 'jwt', // Apply JWT authentication to this route
            auth: false,
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    driverUserId: Joi.string().required().description('User Id of the Driver to be edited'),
                    hospitalId: Joi.string().optional().description('Driver associated Hospital'),
                    ambulanceId: Joi.string().optional().description('Driver associated Ambulance'),
                    fullName: Joi.string().optional().description('Driver fullname'),
                    email: Joi.string().required().description('Driver email'),
                    phoneNumber: Joi.string().required().description('Driver Phone Number'),
                    dob: Joi.string().optional().description('Driver dob'),
                    gender: Joi.string().optional().description('Driver gender'),
                    licenseNumber: Joi.string().optional().description('Driver license'),
                    aadharCardNumber: Joi.string().optional().description('Driver Aadharcard Number'),
                    password: Joi.string().optional().description('User Password'),
                    documentDetails: Joi.array().items(
                        Joi.object().keys({
                            docName: Joi.string(),
                            path: Joi.string(),
                        }),
                    ),
                    isActive: Joi.boolean().optional().description('is active driver status'),
                }),
            },
            description: 'Admin Edit Driver for the website'
        },
        handler: async (request, handler) => {
            const { driverUserId, hospitalId, ambulanceId, fullName, phoneNumber, email, dob, gender, licenseNumber, aadharCardNumber, password, documentDetails, isActive } = request.payload;
            const modifiedAt = new Date().toISOString();
            const modifiedBy = 'admin';

            try {
                // Check if the Driver exists
                let data = {
                    userId: driverUserId
                }
                const existingDriver = await driverStore.getDriver(data);
                // console.log("existingDriver", existingDriver);

                // //get userDetails by userId
                const userDetails = await userStore.getByUserId(driverUserId);
                // console.log("userDetails", userDetails);

                //checking existing Driver
                if (!existingDriver) {
                    return {
                        status: false,
                        statusCode: 404,
                        error: 'Not Found Driver',
                        message: `Please provide valid ${driverUserId} Id`,
                        data: null
                    };
                }

                //checking email and phone number exist or not
                let userEmailExists = await userStore.getByUserEmail(request.payload.email)
                let phoneNumberExists = await userStore.getUserByPhoneNumber(request.payload.phoneNumber)
                if (userDetails.email != request.payload.email) {
                    // console.log("email match");

                    if (userEmailExists) {
                        return handler.response({
                            statusCode: 400,
                            status: false,
                            error: "Failed",
                            message: `Email is already exist`,
                            data: null
                        })
                    }
                }
                if (userDetails.phoneNumber != request.payload.phoneNumber) {
                    // console.log("phoneNumber match");

                    if (phoneNumberExists) {
                        return handler.response({
                            statusCode: 400,
                            status: false,
                            error: 'Failed',
                            message: `Phone number is already exist`,
                            data: null
                        })
                    }
                }
                // console.log("Not email match & Not phone number match");

                // Perform the updated Driver
                const updatedDriver = await driverStore.updateDriverById({
                    hospitalId, ambulanceId, licenseNumber, aadharCardNumber, documentDetails: JSON.stringify(documentDetails),isActive
                }, existingDriver[0].driverId);
                // console.log("updatedDriver", updatedDriver);

                //perfron the user table update
                const updateUser = await userStore.updateusersById({
                    phoneNumber: phoneNumber, email: email, password: password, fullName: fullName,
                    dob: dob, gender: gender,
                }, driverUserId)
                // console.log("updateUser", updateUser);

                // Return success response
                return {
                    statusCode: 200,
                    status: true,
                    error: '',
                    message: 'Driver details has been successfully updated',
                    data: updatedDriver
                };
            } catch (e) {
                console.error('Error updating Driver:', e);
                return {
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while updating the Driver.',
                    data: null
                };
            }
        }
    },
    // Edit Driver add Document Details
    {
        method: 'PATCH',
        path: '/driver/addDocumentDetails',
        config: {
            // auth: 'jwt', // Apply JWT authentication to this route
            auth: false,
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    driverId: Joi.string().required().description('ID of the Driver to be edited'),
                    documentDetails: Joi.array().items(
                        Joi.object().keys({
                            docName: Joi.string(),
                            path: Joi.string(),
                        }),
                    ),
                }),
            },
            description: 'Admin Edit Driver for the website'
        },
        handler: async (request, handler) => {
            const { driverId, documentDetails } = request.payload;
            const modifiedAt = new Date().toISOString();
            const modifiedBy = 'admin';

            try {
                // Check if the Driver exists
                let data = {
                    id: driverId
                }
                const existingDriver = await driverStore.getDriver(data);
                // console.log("existingDriver", existingDriver);
                const userId = existingDriver[0].userId

                //checking existing Driver
                if (!existingDriver) {
                    return {
                        status: false,
                        statusCode: 404,
                        error: 'Not Found',
                        message: `Please provide valid ${driverId} Id`,
                        data: null
                    };
                }

                // Perform the updated Driver
                const updatedDriver = await driverStore.updateDriverById({
                    documentDetails: JSON.stringify(documentDetails)
                }, driverId);
                // console.log("updatedDriver", updatedDriver);

                // Return success response
                return {
                    statusCode: 200,
                    status: true,
                    error: '',
                    message: 'document details has been successfully added in Driver',
                    data: updatedDriver
                };
            } catch (e) {
                // console.error('Error updating Driver:', e);
                return {
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while adding documentDetails of Driver.',
                    data: null
                };
            }
        }
    },
    //patch - Driver remove by id
    {
        method: 'PATCH',
        path: '/driver/remove/{driverUserId}',
        options: {
            auth: false, // Apply JWT authentication to this route
            tags: ['api'],
            description: 'Remove a Driver by ID',
            validate: {
                params: Joi.object({
                    driverUserId: Joi.string().required().description('userId of the Driver to delete')
                })
            }
        },
        handler: async (request, h) => {
            const driverUserId = request.params.driverUserId;
            const modifiedAt = new Date().toISOString();
            const modifiedBy = 'ADMIN';

            try {

                const savedContent = await driverStore.getDriver({ userId: driverUserId });
                console.log("savedContent", savedContent);

                if (!savedContent) {
                    return {
                        status: false,
                        statusCode: 404,
                        error: 'Not Found Driver',
                        message: `Please provide valid ${driverUserId} Id`,
                        data: null
                    };
                }

                // Perform the update
                const updatedDriver = await driverStore.updateDriverByUserId({
                    isActive: false, modifiedAt, modifiedBy
                }, driverUserId);
                console.log("updatedDriver", updatedDriver);

                return {
                    statusCode: 200,
                    status: true,
                    message: `Driver Id ${driverUserId} Removed`,
                    data: updatedDriver
                };

            } catch (error) {
                return ({
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while deleting the Driver.',
                    data: null
                })
            }
        },

    },
];

