const Joi = require('joi');
const short = require('short-uuid');
const bookingStore = require('../stores/bookingStore');

module.exports = [
    //get All BookingData
    {
        method: 'POST',
        path: '/booking/initiate',
        config: {
            auth: false,
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    rideDetails:
                        Joi.array().items(
                            Joi.object({
                                rideInfo: Joi.array().items(
                                    Joi.object({
                                        rideId: Joi.string().required(),
                                        date: Joi.string().required().description('rides date'),
                                        time: Joi.string().optional().description('rides time'),
                                        pickupLocation: Joi.string().optional().description('rides pickupLocation'),
                                        dropLocation: Joi.string().optional().description('rides dropLocation'),
                                        driverName: Joi.string().optional().description('rides driverName'),
                                        driverId: Joi.string().optional().description('rides driverId'),
                                        ambulanceNumber: Joi.string().optional().description('rides ambulanceNumber'),
                                        ambulanceId: Joi.string().optional().description('rides ambulanceId'),
                                        patientName: Joi.string().optional().description('rides patientName'),
                                        hospitalId: Joi.string().optional().description('rides hospitalId'),
                                    }),
                                ),
                                otherDetails:
                                    Joi.object({
                                        paidAmount: Joi.string().description('paidAmount'),
                                        pendingAmount: Joi.string().description('pendingAmount'),
                                    }),
                            }),
                        ),
                    addressId: Joi.string().required(),
                }),
            },
            description: "Customer starts booking process"
        },
        handler: async (request, handler) => {
            try {
                let totalAvailableProducts = [];
                // const { id } = request.auth.credentials
                // let userInfo = await userstore.getByUserId(id)
                let id = "47bb8b62-f6a4-4b44-8970-e7f897034021";
                let { rideDetails, addressId } = request.payload
                let createdAt = (new Date()).toISOString()
                let modifiedAt = (new Date()).toISOString()
                request.payload.createdBy = 'admin'
                let bookingId = short.generate();

                totalAvailableProducts.push(rideDetails);
                let bookingPayload = {
                    userId: id,
                    bookingId: bookingId,
                    rideDetails: JSON.stringify(rideDetails),
                    addressId: addressId,
                    status: 'pending',
                }

                // Save data 
                let saveBooking = await bookingStore.create(bookingPayload);

                return handler.response({
                    statusCode: 200,
                    status: true,
                    error: "",
                    message: 'Ride successfully booked in pending status',
                    data: saveBooking
                })
            }
            catch (e) {
                console.log(e)
                return handler.response({
                    statusCode: 200,
                    status: false,
                    error: e,
                    message: 'Error while booking ride',
                    data: null
                })

            }
        }
    }


    
];





