const Joi = require('joi');
const short = require('short-uuid');
const ridesStore = require('../stores/ridesStore');
const isEmpty = require('../../core/utils/isEmpty');
const { getDriver } = require('../stores/driverStore');
const driverStore = require('../stores/driverStore');
const { sendFcmNotification } = require('../../core/fcmNotification/notification');
const usersStore = require('../stores/usersStore');
const io = require('../../..');
const { sendFcmNotificationToCustomer } = require('../../core/fcmNotification/customerNotification');

module.exports = [
    //get All Rides
    {
        method: 'GET',
        path: '/rides',
        options: {
            tags: ['api'],
            auth: false,
            description: 'Get Rides',
            notes: 'Returns a list of Rides',
        },
        handler: async (request, h) => {
            try {
                const getRidesList = await ridesStore.getRides();
                for (let index = 0; index < getRidesList.length; index++) {
                    const driverId = getRidesList[index].driverId;
                    let getDriverName = await driverStore.getDriver({ id: driverId });
                    getRidesList[index].driverName = getDriverName[0].fullName;
                }

                return h.response({
                    statusCode: 200,
                    status: true,
                    message: `List of all Rides`,
                    data: getRidesList
                });

            } catch (error) {
                console.log(error);
                return h.response({
                    statusCode: 500,
                    status: false,
                    error: 'Internal Server Error',
                    message: 'An error occurred while get all Rides list.',
                    data: null
                })
            }
        },
    },
    //Get By Id Rides
    {
        method: 'GET',
        path: '/rides/{riderId}',
        options: {
            auth: false, // Apply JWT authentication to this route
            tags: ['api'],
            description: 'Get a Rides by ID',
            validate: {
                params: Joi.object({
                    riderId: Joi.string().required().description('ID of the Rides to fetch')
                })
            }
        },
        handler: async (request, h) => {
            const riderId = request.params.riderId;
            try {
                const getRidesList = await ridesStore.getRides({ id: riderId });


                for (let index = 0; index < getRidesList.length; index++) {
                    const driverId = getRidesList[index].driverId;
                    let getDriverName = await driverStore.getDriver({ id: driverId });
                    getRidesList[index].driverName = getDriverName[0].fullName;
                    getRidesList[index].driverPhoneNumber = getDriverName[0].phoneNumber;
                    getRidesList[index].doctorName = getDriverName[0].doctorName;
                    getRidesList[index].doctorPhoneNumber = getDriverName[0].doctorPhoneNumber;
                }

                if (!isEmpty(getRidesList)) {
                    return h.response({
                        statusCode: 200,
                        status: true,
                        message: `Successfully fetched rides details by ride id ${riderId}`,
                        data: getRidesList
                    });
                } else {
                    return h.response({
                        statusCode: 404,
                        status: false,
                        message: `ride with ID ${riderId} not found`,
                        data: null
                    });
                }
            } catch (error) {
                console.error(error);
                return {
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while fetching Hospital data.',
                    data: null
                };
            }
        },
    },
    //Get Rides by customer Id
    {
        method: 'GET',
        path: '/rides/customer/{customerId}',
        options: {
            auth: false, // Apply JWT authentication to this route
            tags: ['api'],
            description: 'Get a Rides by Customer ID',
            validate: {
                params: Joi.object({
                    customerId: Joi.string().required().description('ID of the Rides to fetch')
                })
            }
        },
        handler: async (request, h) => {
            const customerId = request.params.customerId;
            try {
                const getRidesList = await ridesStore.getRides({ userId: customerId,rideStatus:"COMPLETED" });


                for (let index = 0; index < getRidesList.length; index++) {
                    const driverId = getRidesList[index].driverId;
                    let getDriverName = await driverStore.getDriver({ id: driverId });
                    getRidesList[index].driverName = getDriverName[0].fullName;
                    getRidesList[index].driverPhoneNumber = getDriverName[0].phoneNumber;
                    getRidesList[index].doctorName = getDriverName[0].doctorName;
                    getRidesList[index].doctorPhoneNumber = getDriverName[0].doctorPhoneNumber;
                }

                if (!isEmpty(getRidesList)) {
                    return h.response({
                        statusCode: 200,
                        status: true,
                        message: `Successfully fetched rides details by customer Id ${customerId}`,
                        data: getRidesList
                    });
                } else {
                    return h.response({
                        statusCode: 404,
                        status: false,
                        message: `ride with ID ${customerId} not found`,
                        data: null
                    });
                }
            } catch (error) {
                console.error(error);
                return {
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while fetching Hospital data.',
                    data: null
                };
            }
        },
    },
    //Get By Id Ambulance
    {
        method: 'GET',
        path: '/rides/RidesByAmbulanceId/{ambulanceId}',
        options: {
            auth: false, // Apply JWT authentication to this route
            tags: ['api'],
            description: 'Get a Ambulance by ID',
            validate: {
                params: Joi.object({
                    ambulanceId: Joi.string().required().description('ID of the Ambulance to fetch')
                })
            }
        },
        handler: async (request, h) => {
            const ambulanceId = request.params.ambulanceId;
            try {
                const getRidesList = await ridesStore.getRides({ ambulanceId: ambulanceId });
                for (let index = 0; index < getRidesList.length; index++) {
                    const driverId = getRidesList[index].driverId;
                    let getDriverName = await driverStore.getDriver({ id: driverId });
                    getRidesList[index].driverName = getDriverName[0].fullName;
                }
                // console.log("ambulanceDetails", ambulanceDetails);

                if (!isEmpty(getRidesList)) {
                    return h.response({
                        statusCode: 200,
                        status: true,
                        message: `Successfully fetched rides details by ambulance Id ${ambulanceId}`,
                        data: getRidesList
                    });
                } else {
                    return h.response({
                        statusCode: 404,
                        status: false,
                        message: `Ambulance with ID ${ambulanceId} not found`,
                        data: null
                    });
                }
            } catch (error) {
                console.error(error);
                return {
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while fetching Ambulance data.',
                    data: null
                };
            }
        },
    },
    //Get By Id Hospital
    {
        method: 'GET',
        path: '/rides/RidesByHospitalId/{hospitalId}',
        options: {
            auth: false, // Apply JWT authentication to this route
            tags: ['api'],
            description: 'Get a rides hospital by ID',
            validate: {
                params: Joi.object({
                    hospitalId: Joi.string().required().description('ID of the Hospital to fetch')
                })
            }
        },
        handler: async (request, h) => {
            const hospitalId = request.params.hospitalId;
            try {
                const getRidesList = await ridesStore.getRides({ hospitalId: hospitalId });


                for (let index = 0; index < getRidesList.length; index++) {
                    const driverId = getRidesList[index].driverId;
                    let getDriverName = await driverStore.getDriver({ id: driverId });
                    getRidesList[index].driverName = getDriverName[0].fullName;
                }

                if (!isEmpty(getRidesList)) {
                    return h.response({
                        statusCode: 200,
                        status: true,
                        message: `Successfully fetched rides details by hospital id ${hospitalId}`,
                        data: getRidesList
                    });
                } else {
                    return h.response({
                        statusCode: 404,
                        status: false,
                        message: `Hospital with ID ${hospitalId} not found`,
                        data: null
                    });
                }
            } catch (error) {
                console.error(error);
                return {
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while fetching Hospital data.',
                    data: null
                };
            }
        },
    },
    //Get By Status
    {
        method: 'GET',
        path: '/rides/RidesByDriverId/{driverId}',
        options: {
            auth: false, // Apply JWT authentication to this route
            tags: ['api'],
            description: 'Get a rides by driver Id',
            validate: {
                params: Joi.object({
                    driverId: Joi.string().required().description('ID of the Driver to fetch')
                })
            }
        },
        handler: async (request, h) => {
            const driverId = request.params.driverId;
            try {
                const getRidesList = await ridesStore.getRides({ driverId: driverId,rideStatus:"COMPLETED" });
                
                for (let index = 0; index < getRidesList.length; index++) {
                    const driverId = getRidesList[index].driverId;
                    let getDriverName = await driverStore.getDriver({ id: driverId });
                    getRidesList[index].driverName = getDriverName[0].fullName;
                    getRidesList[index].driverPhoneNumber = getDriverName[0].phoneNumber;
                    getRidesList[index].doctorName = getDriverName[0].doctorName;
                    getRidesList[index].doctorPhoneNumber = getDriverName[0].doctorPhoneNumber;
                }

                if (!isEmpty(getRidesList)) {
                    return h.response({
                        statusCode: 200,
                        status: true,
                        message: `Successfully fetched details by driver id ${driverId}`,
                        data: getRidesList
                    });
                } else {
                    return h.response({
                        statusCode: 404,
                        status: false,
                        message: `Driver with ID ${driverId} not found`,
                        data: null
                    });
                }
            } catch (error) {
                console.error(error);
                return {
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while fetching driver data.',
                    data: null
                };
            }
        },
    },
    // GET By ride status
    {
        method: 'GET',
        path: '/rides/getByStatus/{rideStatus}',
        options: {
            auth: false, // Apply JWT authentication to this route
            tags: ['api'],
            description: 'Get a rides by ride status',
            validate: {
                params: Joi.object({
                    // type: Joi.string().valid('GOVERNMENT', 'PRIVATE').required().description('Ambulance type'),
                    rideStatus: Joi.string().valid('PENDING', 'ONGOING','COMPLETED','CANCELLED').required().description('get ride by ride status')
                })
            }
        },
        handler: async (request, h) => {
            const rideStatus = request.params.rideStatus;
            try {
                const getRidesList = await ridesStore.getRides({ rideStatus:rideStatus });
                for (let index = 0; index < getRidesList.length; index++) {
                    const driverId = getRidesList[index].driverId;
                    let getDriverName = await driverStore.getDriver({ id: driverId });
                    getRidesList[index].driverName = getDriverName[0].fullName;
                }

                if (!isEmpty(getRidesList)) {
                    return h.response({
                        statusCode: 200,
                        status: true,
                        message: `Successfully fetched ride details by ride stauts ${rideStatus}`,
                        data: getRidesList
                    });
                } else {
                    return h.response({
                        statusCode: 404,
                        status: false,
                        message: `rides not found with ride stauts ${rideStatus} not found`,
                        data: null
                    });
                }
            } catch (error) {
                console.error(error);
                return {
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while fetching driver data.',
                    data: null
                };
            }
        },
    },
    // Add Rides- old
    {
        method: 'POST',
        path: '/rides/add-old',
        config: {
            // auth: 'jwt', // Apply JWT authentication to this route
            auth: false,
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    userId: Joi.string().optional().description('rides userId'),
                    ambulanceId: Joi.string().optional().description('rides ambulanceId'),
                    driverId: Joi.string().optional().description('rides driverId'),
                    hospitalId: Joi.string().optional().description('rides hospitalId'),
                    pickupPoint: Joi.string().optional().description('rides pickupPoint'),
                    dropoffPoint: Joi.string().optional().description('rides dropoffPoint'),
                    ridesSummary: Joi.array().items(Joi.object({
                        logs: Joi.string().description('logs'),
                        datetime: Joi.string().description('datetime'),
                    }),),
                    locationSummary: Joi.array().items({
                        driver: Joi.object({
                            lat: Joi.string().description('lat'),
                            lang: Joi.string().description('lang'),
                        }),
                        user: Joi.object({
                            lat: Joi.string().description('lat'),
                            lang: Joi.string().description('lang'),
                        }),
                        hospital: Joi.object({
                            lat: Joi.string().description('lat'),
                            lang: Joi.string().description('lang'),
                        })
                    }),
                }),
            },
            description: 'Admin add rides for the website'
        },
        handler: async (request, handler) => {
            let { userId, ambulanceId, driverId, hospitalId, pickupPoint, dropoffPoint, ridesSummary, locationSummary } = request.payload
            let createdAt = new Date().toISOString()
            let createdBy = 'CUSTOMER'
            let bookingId = short.generate();
            let title = 'New Ride Request';
            let body = 'Customer want to book an ambulance';

            try {

                let bookingRidePayload = {
                    userId: userId,
                    booingId: bookingId,
                    ambulanceId: ambulanceId,
                    driverId: driverId,
                    hospitalId: hospitalId,
                    pickupPoint: pickupPoint,
                    dropoffPoint: dropoffPoint,
                    ridesSummary: JSON.stringify(ridesSummary),
                    locationSummary: JSON.stringify(locationSummary),
                    status: 'pending',
                    createdAt: createdAt,
                    createdBy: createdBy
                }

                let saveBooking = await ridesStore.create(bookingRidePayload)
                console.log("++++++++++++++saveBookings++++++++++++++++++");
                console.log(`saveBookings`, saveBooking);
                console.log("++++++++++++++saveBookings++++++++++++++++++");
                // Get userId Details
                let customerDetails = await usersStore.getByUserId(userId);
                console.log("++++++++++++++customerDetails++++++++++++++++++");
                console.log(`customerDetails`, customerDetails);
                console.log("++++++++++++++customerDetails++++++++++++++++++");
                // send notification to driver
                let getDriverDetails = await driverStore.getDriver({id:driverId});
                console.log("++++++++++++++getDriverDetails++++++++++++++++++");
                console.log(`getDriverDetails`, getDriverDetails);
                console.log("++++++++++++++getDriverDetails++++++++++++++++++");
                const notificationPayload = {
                    rideId: saveBooking[0].id,
                    bookingId: saveBooking[0].booingId,
                    customerName: customerDetails.fullName,
                    pickupLocation: pickupPoint,
                    destination: dropoffPoint,
                    isRideNotification:true
                  };
                
                // Send notification to driver for ride   
               await sendFcmNotification(notificationPayload,getDriverDetails[0].fcmToken,title,body);

                return handler.response({
                    statusCode: 200,
                    status: true,
                    error: ``,
                    message: 'Ride successfully booked in pending status',
                    data: saveBooking
                })
            }
            catch (e) {
                console.error('Error Booking rides:', e);
                 return handler.response({
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while Booking the rides.',
                    data: null
                })
            }
        }
    },
     // Add Rides
    {
        method: 'POST',
        path: '/rides/add',
        config: {
            // auth: 'jwt', // Apply JWT authentication to this route
            auth: false,
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    userId: Joi.string().optional().description('rides userId'),
                    AmbulanceType: Joi.string().optional().description('Type of Ambulance'),
                    hospitalId: Joi.string().optional().description('rides hospitalId'),
                    pickupPoint: Joi.string().optional().description('rides pickupPoint'),
                    dropoffPoint: Joi.string().optional().description('rides dropoffPoint'),
                    ridesSummary: Joi.array().items(Joi.object({
                        logs: Joi.string().description('logs'),
                        datetime: Joi.string().description('datetime'),
                    }),),
                    locationSummary: Joi.array().items({
                        driver: Joi.object({
                            lat: Joi.string().description('lat'),
                            lang: Joi.string().description('lang'),
                        }),
                        user: Joi.object({
                            lat: Joi.string().description('lat'),
                            lang: Joi.string().description('lang'),
                        }),
                        hospital: Joi.object({
                            lat: Joi.string().description('lat'),
                            lang: Joi.string().description('lang'),
                        })
                    }),
                }),
            },
            description: 'Admin add rides for the website'
        },
        handler: async (request, handler) => {
            let { userId,AmbulanceType, hospitalId, pickupPoint, dropoffPoint, ridesSummary, locationSummary } = request.payload
            let createdAt = new Date().toISOString()
            let createdBy = 'CUSTOMER'
            let bookingId = short.generate();
            let title = 'New Ride Request';
            let body = 'Customer want to book an ambulance';

            try {

                let bookingRidePayload = {
                    userId: userId,
                    bookingId: bookingId,
                    hospitalId: hospitalId,
                    pickupPoint: pickupPoint,
                    dropoffPoint: dropoffPoint,
                    ridesSummary: JSON.stringify(ridesSummary),
                    locationSummary: JSON.stringify(locationSummary),
                    status: 'PENDING',
                    createdAt: createdAt,
                    createdBy: createdBy
                }

                let saveBooking = await ridesStore.create(bookingRidePayload)
                console.log("++++++++++++++++++++++++++++++++++++++++++++");
                console.log("++++++++++++++1.saveBookings++++++++++++++++++");
                console.log(`saveBookings`, saveBooking);
                console.log("++++++++++++++saveBookings++++++++++++++++++");
                console.log("++++++++++++++++++++++++++++++++++++++++++++");
                // Get userId Details
                let customerDetails = await usersStore.getByUserId(userId);
                console.log("++++++++++++++++++++++++++++++++++++++++++++");
                console.log("++++++++++++++2. get customerDetails++++++++++++++++++");
                console.log(`customerDetails`, customerDetails);
                console.log("++++++++++++++customerDetails++++++++++++++++++");
                console.log("++++++++++++++++++++++++++++++++++++++++++++");

                // send notification to driver based on hospital ID and type of ambulance

                let getOnlineDrivers = await driverStore.getDriver({hospitalId:hospitalId,ambulanceType:AmbulanceType})
                console.log("++++++++++++++++++++++++++++++++++++++++++++");
                console.log("++++++++++++++ 3.get Online DriverDetails and sent notifiction++++++++++++++++++");
                console.log(`getOnlineDrivers`, getOnlineDrivers);
                console.log("++++++++++++++get Online DriverDetails++++++++++++++++++");
                console.log("++++++++++++++++++++++++++++++++++++++++++++");

                const filteredTokens = getOnlineDrivers.filter(obj => obj.fcmToken !== null).map(obj => obj.fcmToken);
                console.log(filteredTokens);

                const filteredTokensLength = filteredTokens.length                

                // console.log("++++++++++++++++++++++++++++++++++++++++++++++++++");
                // console.log("++++++++++++++Rides Booking API++++++++++++++++++");
                // console.log(saveBooking);
                // console.log("++++++++++++++Rides Booking API++++++++++++++++++");
                // console.log("++++++++++++++++++++++++++++++++++++++++++++++++++");
              

                if(filteredTokensLength == 0) {
                    return handler.response({
                        statusCode: 200,
                        status: true,
                        error: ``,
                        message: 'No driver avaialble for the booking....',
                        data: []
                    })
                } else {

                const notificationPayload = {
                    rideId: saveBooking[0].id,
                    bookingId: saveBooking[0].bookingId,
                    customerName: customerDetails.fullName,
                    pickupLocation: pickupPoint,
                    destination: dropoffPoint,
                    isRideNotification:true
                  };
                
                // Send notification to driver for ride   
                let appType = 'driver';
                await sendFcmNotificationToCustomer(notificationPayload,filteredTokens,title,body,appType);
                
                console.log("++++++++++++++++++++++++++++++++++++++++++++++++++");
                console.log("++++++++++++++Rides Booking API+++++++++++++++++++");
                console.log(saveBooking);
                console.log("++++++++++++++Rides Booking API+++++++++++++++++++");
                console.log("++++++++++++++++++++++++++++++++++++++++++++++++++");


                return handler.response({
                    statusCode: 200,
                    status: true,
                    error: ``,
                    message: 'Ride successfully booked in pending status',
                    data: saveBooking
                })
            }
            }
            catch (e) {
                console.error('Error Booking rides:', e);
                 return handler.response({
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while Booking the rides.',
                    data: null
                })
            }
        }
    },
    // Edit Rides
    {
        method: 'PATCH',
        path: '/rides/edit/ridesSummary',
        config: {
            // auth: 'jwt', // Apply JWT authentication to this route
            auth: false,
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    rideId: Joi.string().required().description('Id of the ride'),
                    ridesSummary: Joi.array().items(Joi.object({
                        logs: Joi.string().description('logs'),
                        datetime: Joi.string().description('datetime'),
                    }),),
                }),
            },
            description: 'Admin add rides for the website'
        },
        handler: async (request, handler) => {
            const { rideId, ridesSummary } = request.payload;
            const modifiedAt = new Date().toISOString();
            const modifiedBy = 'admin';

            try {

                let data = {
                    id: rideId
                }
                // Check if the ride exists
                const existingRide = await ridesStore.getRides(data);
                console.log("existingRide", existingRide);

                if (isEmpty(existingRide)) {
                    return handler.response( {
                        status: false,
                        statusCode: 404,
                        error: 'Not Found',
                        message: 'Ride not found',
                        data: null
                    })
                }

                // Perform the update
                const updatedRide = await ridesStore.updateRidesById({
                    ridesSummary: JSON.stringify(ridesSummary), modifiedAt, modifiedBy
                }, rideId);

                // return  success response
                return handler.response( {
                    statusCode: 200,
                    status: true,
                    error: '',
                    message: 'Ride updated',
                    data: updatedRide
                })
            } catch (e) {
                console.error('Error updating ride:', e);
                return handler.response( {
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while updating the ride.',
                    data: null
                })
            }
        }
    },
    // Edit Rides by
    {
        method: 'PATCH',
        path: '/rides/edit/driverLocationSummary',
        config: {
            // auth: 'jwt', // Apply JWT authentication to this route
            auth: false,
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    rideId: Joi.string().required().description('Id of the ride'),
                    driver: Joi.object({
                        lat: Joi.string().description('lat'),
                        lang: Joi.string().description('lang'),
                    }),
                }),
            },
            description: 'Admin add rides for the website'
        },
        handler: async (request, handler) => {
            const { rideId, driver } = request.payload;
            const modifiedAt = new Date().toISOString();
            const modifiedBy = 'admin';
            
            try {

                let data = {
                    id: rideId
                }
                // Check if the ride exists
                const existingRide = await ridesStore.getRides(data);
                // console.log("existingRide", existingRide);

                if (isEmpty(existingRide)) {
                    return handler.response( {
                        status: false,
                        statusCode: 404,
                        error: 'Not Found',
                        message: 'Ride not found',
                        data: null
                    })
                }

                let locationSummary = [
                    {
                        driver: driver,
                        user: existingRide[0].locationSummary[0].user,
                        hospital: existingRide[0].locationSummary[0].hospital
                    }
                ]

                // Perform the update
                const updatedRide = await ridesStore.updateRidesById({
                    locationSummary: JSON.stringify(locationSummary), modifiedAt, modifiedBy
                }, rideId);

                // return success response
                return handler.response( {
                    statusCode: 200,
                    status: true,
                    error: '',
                    message: 'Ride updated',
                    data: updatedRide
                })
            } catch (e) {
                console.error('Error updating ride:', e);
                return handler.response( {
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while updating the ride.',
                    data: null
                })
            }
        }
    },
    // Edit Rides by status
    {
        method: 'PATCH',
        path: '/rides/edit/stauts',
        config: {
            // auth: 'jwt', // Apply JWT authentication to this route
            auth: false,
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    rideId: Joi.string().required().description('Id of the ride'),
                    driverId: Joi.string().required().description('Id of the ride'),
                    status: Joi.string().required().description('Add Ride Status')
                }),
            },
            description: 'Admin update ride stauts'
        },
        handler: async (request, handler) => {
            const { rideId, driverId,status } = request.payload;
            const modifiedAt = new Date().toISOString();
            const modifiedBy = 'admin';
            let title = "Booking Confirm";
            let body = "Soon Driver will reach at your location";
            // let roomName = "testroom";
            try {

                let data = {
                    id: rideId
                }
                // Check if the ride exists
                const existingRide = await ridesStore.getRides(data);

                console.log(`ride data`,existingRide);

                if (isEmpty(existingRide)) {
                    return handler.response( {
                        status: false,
                        statusCode: 404,
                        error: 'Not Found',
                        message: 'Ride not found',
                        data: null
                    })
                } 

                const rideStaus = existingRide[0].rideStatus;
                const allotedDriverId = existingRide[0].driverId;
                    
                if(((rideStaus === 'ONGOING') || (rideStaus === 'COMPLETED')) && (allotedDriverId != driverId)) {
                    return handler.response( {
                        status: false,
                        statusCode: 404,
                        error: 'Not allowed',
                        message: 'This ride is already alloted to another driver',
                        data: null
                })
                } else {
                    
                   
                // get the ambulance Id 
                const getAmbulanceofDriver = await driverStore.getDriver({id:driverId});
                
                const ambulanceId = getAmbulanceofDriver[0].ambulanceId;
                const driverName = getAmbulanceofDriver[0].fullName;
                const driverPhoneNumber = getAmbulanceofDriver[0].phoneNumber;
                const doctorName = getAmbulanceofDriver[0].doctorName;
                const doctorPhoneNumber = getAmbulanceofDriver[0].doctorPhoneNumber;
                // Perform the update

                // allot driver and ambulance
               
                const updatedRide = await ridesStore.updateRidesById({
                    status,driverId,ambulanceId, modifiedAt, modifiedBy
                }, rideId);
 

                console.log("++++++++++++++++++++++++++++++++++++++++++++++++");
                console.log("++++++allot driver and ambulance to it++++++++++");
                console.log(updatedRide);
                console.log("++++++++++++++customerDetails++++++++++++++++++");
                console.log("+++++++++++++++++++++++++++++++++++++++++++++++");

                // get customer data 
                let customerData = await usersStore.getusers({id:updatedRide[0].userId});

                existingRide[0].driverName = driverName,
                existingRide[0].driverPhoneNumber = driverPhoneNumber
                existingRide[0].doctorName = doctorName
                existingRide[0].docotrPhoneNumber = doctorPhoneNumber

                // socket code to send live data
                // create socket room Id : customerId + _ + driverId
                const roomId = `${existingRide[0].customerId}_${driverId}`; // create Room ID
                if(status === 'ONGOING') {
                    
                    let message = existingRide[0];
                    existingRide[0].roomId = roomId; // add roomId Into object
                    let notificationPayload = {
                        id: "test"

                    }

                    // send notification to user
                    let customerToken = [];
                    customerToken.push(customerData[0].fcmToken)

                    console.log(`user notification token`);
                    console.log(customerToken);
                    let appType = 'customer';
                    await sendFcmNotificationToCustomer(existingRide[0],customerToken,title,body,appType);
 
                    // socket IO connection
                    io.on('connection', (socket) => {
                        socket.join(roomId); // create or join a room
                        io.emit(roomId,{ room: roomId, message }); // send the message to client
                       
                        socket.on(roomId, ({ room, message }) => {  // receive message to from client
                        console.log(`message receieved from client having room id ${room}`);
                        // get the client message and perform the operations as per the logic
                        // console.log('client message...', message)
                        // console.log('room', room)
                    });

                    socket.on('disconnect', () => {
                            console.log(`A user disconnected: ${socket.id}`);
                    });
                    });

                // return success response
                return handler.response( {
                    statusCode: 200,
                    status: true,
                    error: '',
                    message: `Ride status ${status} updated and Created or joined room: ${roomId}`,
                    data: existingRide
                })
   
                } else {
                    return handler.response( {
                        statusCode: 200,
                        status: true,
                        error: '',
                        message: `Ride status ${status} updated`,
                        data: existingRide
                    })
                }
                
            }
            } catch (e) {
                console.error('Error updating ride:', e);
                return handler.response( {
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while updating the ride.',
                    data: null
                })
            }
        }
    },
    // Test Notification API
    {
        method: 'POST',
        path: '/rides/test-notification',
        config: {
            // auth: 'jwt', // Apply JWT authentication to this route
            auth: false,
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    fcmToken: Joi.string().optional().description('FCM TOKEN of device'),
                    title: Joi.string().optional().description('FCM title'),
                    body: Joi.string().optional().description('FCM body'),
                    appType: Joi.string().valid('customer', 'driver').description('app type')
                }),
            },
            description: 'Test Notification'
        },
        handler: async (request, handler) => {
            let { fcmToken,title,body,appType } = request.payload
            let customerToken = [];
            customerToken.push(fcmToken)
            let notificationStatus ;

            try {
                const customData = {
                    "rideId":"ede1f5c9-444c-4da5-a3bf-257407b9e436","bookingId":'tyxhy3pKvmStHie5RSszH1',"customerName":"vedvrat Deshpande","pickupLocation":"Pickup Address 2","destination":"Drop-off Address 2","isRideNotification":true};
                
                    if(1) {

                        let customerToken = [];
                        customerToken.push(fcmToken)
                        notificationStatus = await sendFcmNotificationToCustomer(customData,customerToken,title,body,appType);
                    } 
                console.log("Notification Status", notificationStatus);
                if(notificationStatus == `Notification sent successfully`) {
                    return handler.response({
                        statusCode: 200,
                        status: true,
                        error: ``,
                        message: 'Notification send successfully',
                        data: []
                    }) 
                }  else {
                    return handler.response({
                        statusCode: 200,
                        status: true,
                        error: ``,
                        message: 'Notification send successfully',
                        data: []
                    }) 
                }
            }
            catch (e) {
                console.error('Error Booking rides:', e);
                 return handler.response({
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while sending notification',
                    data: null
                })

            }

        }
    },
     //Get room connection
    {
        method: 'GET',
        path: '/rides/room-connection/',
        options: {
            auth: false, // Apply JWT authentication to this route
            tags: ['api'],
            description: 'Get a rides by driver Id',
        },
        handler: async (request, h) => {
            // const driverId = request.params.driverId;
            let roomId = '1234';
            let message = "Hello, Server! rides routess";
            try {
               
                io.emit('receivedMessage', { sender: 'backend server', message });
                
                
                // console.log(io.emit('sendToRoom', { room: 'roomName', message: 'Hello, Server! rides routess' }));
                return `Room created with ID: ${roomId}`;
            } catch (error) {
                console.error(error);
                return {
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while fetching driver data.',
                    data: null
                };
            }
        },
    },
];





