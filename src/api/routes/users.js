const Joi = require('joi');
const usersStore = require('../stores/usersStore');

module.exports = [
    //get users data by : scope 
    {
        method: 'GET',
        path: '/usersBy/{scope}',
        options: {
            tags: ['api'],
            auth: false,
            description: 'Get Users',
            validate: {
                params: Joi.object({
                    scope: Joi.string().required().description('user scope')
                })
            },
            notes: 'Returns a list of Users',
        },
        handler: async (request, h) => {
            const userScope = request.params.scope;
            try {
                const getUsersList = await usersStore.getusers({ scope: userScope });
                return h.response({
                    statusCode: 200,
                    status: true,
                    error: ``,
                    message: `All user data having ${userScope} scope`,
                    data: getUsersList
                })
            } catch (error) {
                console.error('Database error:', error);
                return h.response('An error occurred').code(500);
            }
        },
    },
    //Add Users
    {
        method: 'POST',
        path: '/users/add',
        config: {
            auth: {
                access: {
                    scope: ['admin']
                }
            },// Apply JWT authentication to this route
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    fullName: Joi.string().required().description('User'),
                    email: Joi.string().optional().description('User'),
                    phoneNumber: Joi.string().optional().description('User'),
                    company: Joi.string().optional().description('User'),
                    role: Joi.string().optional().description('User'),
                    password: Joi.string().optional().description('User'),
                    signupType: Joi.string().optional().description('User'),
                }),
            },
            description: 'Admin add User for the website'
        },
        handler: async (request, handler) => {
            let { fullName, email, phoneNumber, company, role, password, signupType } = request.payload
            let createdAt = new Date().toISOString()
            let createdBy = 'admin'
            try {
                let savedContent = await usersStore.create({
                    fullName, email, phoneNumber, company, role, password, signupType, createdAt, createdBy
                })
                console.log("savedContent", savedContent);
                return ({
                    statusCode: 200,
                    status: true,
                    error: ``,
                    message: 'User Added',
                    data: savedContent
                })
            }
            catch (e) {
                console.error('Error adding user:', e);
                return ({
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while adding the user.',
                    data: null
                })

            }

        }
    },
    //Delete Users
    {
        method: 'DELETE',
        path: '/users/{userId}',
        options: {
            auth: {
                access: {
                    scope: ['admin']
                }
            }, // Apply JWT authentication to this route
            tags: ['api'],
            description: 'Delete a user by ID',
            validate: {
                params: Joi.object({
                    userId: Joi.string().required().description('ID of the user to delete')
                })
            }
        },
        handler: async (request, h) => {
            const userId = request.params.userId;
            try {
                const savedContent = await usersStore.deleteusersById(userId);
                return {
                    statusCode: 200,
                    status: true,
                    message: `User ${userId} deleted`,
                    data: savedContent
                };
            } catch (error) {
                return ({
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while updating the user.',
                    data: null
                })
            }
        },

    },
    //Get By Id Users
    {
        method: 'GET',
        path: '/users/{userId}',
        options: {
            auth: false, // Apply JWT authentication to this route
            // auth: {
            //     access: {
            //         scope: ['admin']
            //     }
            // },
            tags: ['api'],
            description: 'Get a user by ID',
            validate: {
                params: Joi.object({
                    userId: Joi.string().required().description('ID of the user to fetch')
                })
            }
        },
        handler: async (request, h) => {
            const userId = request.params.userId;
            try {
                const userDetails = await usersStore.getusers(userId);
                if (userDetails && userDetails.length > 0) {

                    const user = userDetails.find(user => user.id === userId);
                    console.log("user", user);



                    if (user) {
                        return {
                            statusCode: 200,
                            status: true,
                            message: `User ID ${userId} Data`,
                            data: user
                        };
                    } else {
                        return {
                            statusCode: 404,
                            status: false,
                            message: `User with ID ${userId} not found`,
                            data: null
                        };
                    }
                } else {
                    return {
                        statusCode: 404,
                        status: false,
                        message: `User with ID ${userId} not found`,
                        data: null
                    };
                }
            } catch (error) {
                return {
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while fetching user data.',
                    data: null
                };
            }
        },
    },
];





