const Joi = require('joi');
const hospitalStore = require('../stores/hospitalStore');
const ridesStore = require('../stores/ridesStore');
const ambulanceStore = require('../stores/ambulanceStore');
const driverStore = require('../stores/driverStore');


module.exports = [

    // GET Dashboard status
    {
        method: 'GET',
        path: '/admin/dashboard/status',
        config: {
            auth: false,
            tags: ['api'],
            description: 'Admin dashboard status'
        },
        handler: async (request, handler) => {
            let getAllAmbulanceRidesData = [];

            try {

                // rides count
                let getAllRidesCount = await ridesStore.getAllRidesCount();

                // Hospital count
                let getAllHospitalCount = await hospitalStore.getAllHospitalCount();
                let getGovernmentHospitalCount = await hospitalStore.getGovernmentHospitalCount();
                let getPrivateHospitalCount = await hospitalStore.getPrivateHospitalCount();
            
                //Ambulance
                let getAllAmbulanceCount = await ambulanceStore.getAllAmbulanceCount();
                let getBasicAmbulanceCount = await ambulanceStore.getBasicAmbulanceCount();
                let getAdvanceAmbulanceCount = await ambulanceStore.getAdvanceAmbulanceCount();
                let getMaternityAmbulanceCount = await ambulanceStore.getMaternityAmbulanceCount();

                getAllAmbulanceRidesData = await ridesStore.getRidesByAmbulanceType();

                let ambulanceTypeCounts = {
                    "BASIC": 0,
                    "ADVANCE": 0,
                    "MATERNITY": 0
                };
                for (let i = 0; i < getAllAmbulanceRidesData.length; i++) {
                    const ride = getAllAmbulanceRidesData[i];
                    ambulanceTypeCounts[ride.type]++;
                }

                // Hospital count
                let getAllDriverCount = await driverStore.getAllDriverCount();


                let response = {
                    totalRidesCount: +getAllRidesCount[0].count,
                    totalHospitalCount: +getAllHospitalCount[0].count,
                    totalGovernmentHospitalCount: +getGovernmentHospitalCount[0].count,
                    totalPrivateHospitalCount: +getPrivateHospitalCount[0].count,
                    totalDriverCount: +getAllDriverCount[0].count,
                    totalAmbulanceCount: +getAllAmbulanceCount[0].count,
                    totalBasicAmbulanceCount: +getBasicAmbulanceCount[0].count,
                    totalAdvanceAmbulanceCount: +getAdvanceAmbulanceCount[0].count,
                    totalMaternityAmbulanceCount: +getMaternityAmbulanceCount[0].count,
                    totalRidesByAmbulanceType: ambulanceTypeCounts,
                }

                return handler.response({
                    status: true,
                    statusCode: 200,
                    error: '',
                    message: 'Admin dashboard status',
                    data: response
                })
            }
            catch (e) {
                console.error('Error Admin dashboard status:', e);
                return handler.response({
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while Admin dashboard status.',
                    data: null
                })
            }
        }
    }

];