const Joi = require('joi');
const userstore = require('../stores/usersStore');
const { Signup, SignupOtp } = require('../../core/auth/signup');
const { generateRandomToken } = require('../../core/token');
const { generateOtp } = require('../../core/auth/otp');
const uservalidationstore = require('../stores/usersValidationStore');
const axios = require('axios');
const isEmpty = require('../../core/utils/isEmpty');
const driverStore = require('../stores/driverStore');
const { date } = require('joi');

module.exports = [

    // POST - Customer - Signup otp
    {
        method: 'POST',
        path: '/customer/Signup',
        config: {
            auth: false,
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    fullName: Joi.string().description('Customer full name'),
                    phoneNumber: Joi.number().required().description('The phone number of the Customer'),
                    fcmToken: Joi.number().optional().description('add firebase token'),
                }),
            },
            description: 'Authenticate a admin and customer returns an auth cookie'
        },
        handler: async (request, handler) => {
            let phoneNumber = request.payload.phoneNumber
            let userRole = 'CUSTOMER';;
            let createdAt = new Date().toISOString()

            var response, responseOtp;
            let token = generateRandomToken(32)
            try {

                let userExists = await userstore.getUserByPhoneNumber(phoneNumber);
               // console.log("userExists", userExists);
              //  console.log("userExists", userExists.id);

                if (userExists != null) {
                
                //user exist, so send otp for login

                responseOtp = await generateOtp()
                // console.log("responseOtp", responseOtp);

                let otpObj = await generateOtp()
                // await uservalidationstore.createUserValidation({ userId: userExists.id, createdBy: 'Admin', emailValidationKey: token, active: true, createdBy: 'Admin' })
                await uservalidationstore.update(userExists.id, otpObj)


                return handler.response({
                            statusCode: 200,
                            status: true,
                            error: '',
                            message: 'Login OTP send successfully',
                            data: null
                        })
                    
                } else {

                    let { authToken, user } = await SignupOtp({ ...request.payload }, userRole)
                    // console.log("authToken", authToken);
                    // console.log("user", user);

                    // Create a token 32 char length for email verification
                    // let token = generateRandomToken(32)

                    // OTP Update
                    let CustomerInfo = await userstore.getByUserId(user.data.id)

                    if (CustomerInfo != null) {

                        responseOtp = await generateOtp()
                        // console.log("responseOtp", responseOtp);

                        let otpObj = await generateOtp()
                        await uservalidationstore.createUserValidation({ userId: user.data.id, createdBy: 'Admin', emailValidationKey: token, active: true, createdBy: 'Admin' })
                        await uservalidationstore.update(CustomerInfo.id, otpObj)

                        console.log("CustomerInfo", CustomerInfo);

                        // let mobile = +91 + " " + phoneNumber;

                        // const apiKey = "NjI2ODMxNDI2YTMwNGQzMDcwNDM0YTYyNGY3NjZmNmY="
                        // const sender = "KTRSTR"
                        // const number = mobile

                        // let otp = responseOtp.otp
                        // const message = encodeURIComponent(`Dear Customer,%nYour OTP for login to Kutir Store is ${otp}- Kutir Store`);

                        // var url = "http://api.textlocal.in/send/?" + 'apiKey=' + apiKey + '&sender=' + sender + '&numbers=' + number + '&message=' + message

                        // await axios
                        //     .get(url)
                        //     .then(function (response) {
                        //         console.log("response ", response.data);
                        //     })
                        //     .catch(function (error) {
                        //         console.log("error ", error.message);
                        //         response = error;
                        //     });

                        return handler.response({
                            status: true,
                            statusCode: 200,
                            error: '',
                            message: 'Login OTP send successfully',
                            data: 'Login OTP send successfully'
                        })

                    } else {
                        return handler.response({
                            statusCode: 200,
                            status: true,
                            error: `Signup Failed`,
                            message: 'Customer not found',
                            data: CustomerInfo
                        })
                    }
                }

            } catch (e) {
                console.log(e)
                return handler.response({
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while signup',
                    data: response
                })
            }
        }
    },
    // customer login with phone Otp
    {
        method: 'POST',
        path: '/customer/login',
        config: {
            auth: false,
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    phoneNumber: Joi.string().required(),
                }),
            },
            description: 'Authenticate a admin and customer returns an auth cookie'
        },
        handler: async (request, handler) => {
            try {
                let { phoneNumber } = request.payload
                var response;

                let userExists = await userstore.getUserByPhoneNumber(phoneNumber);
                // console.log("userExists", userExists);

                if (isEmpty(userExists)) {
                    return handler.response({
                        statusCode: 200,
                        status: false,
                        error: 'Phone number not exist',
                        message: 'Phone number not exist',
                        data: []
                    })
                } else {

                    if (userExists.approved != true) {
                        return handler.response({
                            status: true,
                            statusCode: 200,
                            error: '',
                            message: 'Phone number verification Failed, Your account is Pending',
                            data: []
                        })
                    }

                    let otpObj = await generateOtp()
                    await uservalidationstore.update(userExists.id, otpObj)
                    // let mobile = 91 + phoneNumber;

                    // const apiKey = "NjI2ODMxNDI2YTMwNGQzMDcwNDM0YTYyNGY3NjZmNmY="
                    // const sender = "KTRSTR"
                    // const number = mobile

                    // let otp = otpObj.otp
                    // const message = encodeURIComponent(`Dear Customer,%nYour OTP for login to Kutir Store is ${otp}- Kutir Store`);

                    // var url = "http://api.textlocal.in/send/?" + 'apiKey=' + apiKey + '&sender=' + sender + '&numbers=' + number + '&message=' + message

                    // await axios
                    //     .get(url)
                    //     .then(function (response) {
                    //         console.log("response ", response.data);
                    //         response = response;
                    //     })
                    //     .catch(function (error) {
                    //         console.log("error ", error.message);
                    //         response = error;
                    //     });


                    return handler.response({
                        status: true,
                        statusCode: 200,
                        error: '',
                        message: 'OTP send successfully',
                        data: 'OTP send successfully'
                    })
                }

            } catch (e) {
                console.log(e)
                return handler.response({
                    status: false,
                    statusCode: 500,
                    error: '',
                    message: 'Internal Server Error',
                    data: 'An error occurred while login'
                })
            }
        }
    },
    // PATCH Customer - Signup otp Verify
    {
        method: 'PATCH',
        path: '/customer/otp/verify',
        config: {
            auth: false,
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    phoneNumber: Joi.number().required().description('The phone number of the Customer'),
                    otp: Joi.number().required("Enter valid OTP"),
                }),
            },
            description: 'Admin Update User Status'
        },
        handler: async (request, handler) => {
            try {
                const { phoneNumber, otp } = request.payload

                let userDetails = await userstore.getUserByPhoneNumber(phoneNumber);
                // console.log("userDetails", userDetails);

                if (userDetails == null) {
                    return handler.response({
                        statusCode: 200,
                        status: false,
                        error: 'Phone number not exist',
                        message: 'Phone number not exist',
                        data: null
                    })
                }

                let userValidation = await uservalidationstore.getByUserId(userDetails.id)
                // console.log("userValidation", userValidation);

                if (userValidation.approved == false) {
                    // console.log(userValidation.otp + "=" + otp);
                    //update user Table Status
                    let updatedUser = await userstore.updateusersById
                        ({
                            approved: true
                        }, userDetails.id)

                    if (userValidation.otp == otp) {

                        let createdBy = userDetails.fullName
                        let createdAt = new Date().toISOString()

                        let updateUser = await userstore.updateusersById
                            ({
                                approved: true, createdAt, createdBy
                            }, userDetails.id)

                            // console.log(`updateUser`,updateUser);

                        delete updateUser[0].password;    

                        if(updateUser[0].scope[0] == "driver"){
                            // console.log(`in driver login`);
                           let driverData = await driverStore.getDriver({userId:updateUser[0].id})
                        //    console.log(`driver data`,driverData[0].driverId);
                           updateUser[0].driverId = driverData[0].driverId;

                        }


                        return handler.response({
                            status: true,
                            statusCode: 200,
                            error: '',
                            message: 'OTP Verification Successfully',
                            data: updateUser
                        })

                    } else {
                        return handler.response({
                            status: true,
                            statusCode: 200,
                            error: '',
                            message: 'Invalid OTP. Please enter correct OTP',
                            data: []
                        })
                    }
                } else {

                    if (userValidation.otp == otp) {

                        let createdBy = userDetails.fullName
                        let createdAt = new Date().toISOString()

                        let updateUser = await userstore.updateusersById
                            ({
                                approved: true, createdAt, createdBy
                            }, userDetails.id)
                         // console.log(`updateUser`,updateUser);
                        delete updateUser[0].password;

                        if(updateUser[0].scope[0] == "driver"){
                            // console.log(`in driver login`);
                           let driverData = await driverStore.getDriver({userId:updateUser[0].id})
                        //    console.log(`driver data`,driverData[0].driverId);
                           updateUser[0].driverId = driverData[0].driverId;

                        }

                        return handler.response({
                            status: true,
                            statusCode: 200,
                            error: '',
                            message: 'OTP Verification Successfully',
                            data: updateUser
                        })

                    } else {
                        return handler.response({
                            status: true,
                            statusCode: 200,
                            error: '',
                            message: 'Invalid OTP. Please enter correct OTP',
                            data: []
                        })
                    }
                }
            } catch (e) {
                console.log(e)
                return handler.response({
                    status: false,
                    statusCode: 200,
                    error: 'Falied To OTP Verification',
                    message: 'Falied To OTP Verification,Phone Number Not Exist',
                    data: null
                })

            }

        }
    },
    // PATCH API -- UPDATE FCM TOKEN : DRIVER
    {
        method: 'PATCH',
        path: '/users/fcmToken',
        config: {
            auth: false,
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    userId: Joi.string().required().description('Any user Id'),
                    fcmToken: Joi.string().required("enter FCM TOKEN"),
                }),
            },
            description: 'Update User FCM Token'
        },
        handler: async (request, handler) => {
            try {
                const { userId, fcmToken } = request.payload

                let userDetails = await userstore.getByUserId(userId);
                // console.log("userDetails", userDetails);

                if (userDetails == null) {
                    return handler.response({
                        statusCode: 200,
                        status: false,
                        error: 'User not Exist',
                        message: 'User not Exist',
                        data: null
                    })
                }

                let updatedUser = await userstore.updateusersById({fcmToken: fcmToken}, userDetails.id)
                console.log(`++++++++++++++++user update : FCM Token for user ++++++++++++++++++++`);
                // console.log(updatedUser);
                console.log(`++++++++++++++++user update : FCM Token++++++++++++++++++++`);
                return handler.response({
                    status: true,
                    statusCode: 200,
                    error: '',
                    message: 'FCM Token updated successfully',
                    data: updatedUser
                });
            } catch (e) {
                console.log(e)
                return handler.response({
                    status: false,
                    statusCode: 200,
                    error: 'Falied To OTP Verification',
                    message: 'Falied To OTP Verification,Phone Number Not Exist',
                    data: null
                })

            }

        }
    },



];
