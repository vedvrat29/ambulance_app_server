const Joi = require('joi');
const isEmpty = require('../../core/utils/isEmpty');
const ambulanceStore = require('../stores/ambulanceStore');
const hospitalStore = require('../stores/hospitalStore');
const driverStore = require('../stores/driverStore');

module.exports = [
    //get All Ambulance
    {
        method: 'GET',
        path: '/ambulance',
        options: {
            tags: ['api'],
            auth: false,
            description: 'Get Ambulance',
            notes: 'Returns a list of Ambulance',
        },
        handler: async (request, handler) => {
            try {
                const getAmbulanceList = await ambulanceStore.getAmbulances();
                return handler.response({
                    statusCode: 200,
                    status: true,
                    message: `List of all Ambulance`,
                    data: getAmbulanceList
                });
            } catch (error) {
                console.error('Database error:', error);
                return handler.response({
                    statusCode: 500,
                    status: false,
                    error: 'Internal Server Error',
                    message: 'An error occurred while get all Ambulance list.',
                    data: null
                })
            }
        },
    },
    //Add Ambulance
    {
        method: 'POST',
        path: '/ambulance/add',
        config: {
            // auth: 'jwt', // Apply JWT authentication to this route
            auth: false,
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    hospitalId: Joi.string().description('Ambulance Id'),
                    type: Joi.string().valid('BASIC', 'ADVANCE', 'MATERNITY').required().description('Ambulance type'),
                    numberPlate: Joi.string().optional().description('Ambulance number')
                }),
            },
            description: 'Admin add Ambulance for the website'
        },
        handler: async (request, handler) => {
            let { hospitalId, type, numberPlate } = request.payload
            //By defauly ambulance will ACTIVE afer creation.
            let status = 'ACTIVE';
            let createdAt = new Date().toISOString()
            let createdBy = 'ADMIN'
            let existNumberPlate = []
            try {

                existNumberPlate = await ambulanceStore.getAmbulanceByNumberPlate(numberPlate);
                if (existNumberPlate.length != 0) {
                    return handler.response({
                        "statusCode": 400,
                        "status": false,
                        "error": "Failed",
                        "message": "This number plate is already present",
                        "data": null
                    })
                }

                let savedContent = await ambulanceStore.create({
                    hospitalId, type, numberPlate, status, createdAt, createdBy
                })

                return handler.response({
                    statusCode: 200,
                    status: true,
                    error: ``,
                    message: 'New Ambulance details has been successfully added',
                    data: savedContent
                })

            }
            catch (e) {
                console.error('Error adding Ambulance:', e);
                return handler.response({
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while adding the Ambulance.',
                    data: null
                })
            }
        }

    },
    // Edit Ambulance
    {
        method: 'PATCH',
        path: '/ambulance/edit',
        config: {
            // auth: 'jwt', // Apply JWT authentication to this route
            auth: false,
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    ambulanceId: Joi.string().required().description('ID of the Ambulance to be edited'),
                    type: Joi.string().valid('BASIC', 'ADVANCE', 'MATERNITY').required().description('Ambulance type'),
                    numberPlate: Joi.string().optional().description('Ambulance numberPlate'),
                    hospitalId: Joi.string().optional().description('Ambulance hospitalId'),
                    status: Joi.string().optional().description('Ambulance status'),
                    isActive: Joi.boolean().optional().description('is active ambulance status'),
                }),
            },
            description: 'Admin Edit Ambulance for the website'
        },
        handler: async (request, handler) => {
            const { ambulanceId, type, numberPlate, hospitalId, status, isActive } = request.payload;
            const modifiedAt = new Date().toISOString();
            const modifiedBy = 'admin';

            try {

                // Check if the ride exists
                let data = {
                    id: ambulanceId
                }
                const existingRide = await ambulanceStore.getAmbulances(data);
                // console.log("existingRide", existingRide);
                if (!existingRide) {
                    return handler.response({
                        status: false,
                        statusCode: 404,
                        error: 'Internal Server Error',
                        message: "This number plate is already present",
                        data: null
                    })
                }

                // Perform the update  ------------driverId: JSON.stringify(driverId)---------------driverId: driverId
                const updatedAmbulance = await ambulanceStore.updateAmbulanceById({
                    type, numberPlate, hospitalId, status, modifiedAt, modifiedBy,isActive
                }, ambulanceId);

                // Return success response
                return handler.response({
                    statusCode: 200,
                    status: true,
                    error: '',
                    message: 'Ambulance details has been successfully updated',
                    data: updatedAmbulance
                })
            } catch (e) {
                console.error('Error updating Ambulance:', e);
                return handler.response({
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while updating the Ambulance.',
                    data: null
                })
            }
        }
    },
    //Get By Id Ambulance
    {
        method: 'GET',
        path: '/ambulance/{ambulanceId}',
        options: {
            auth: false, // Apply JWT authentication to this route
            tags: ['api'],
            description: 'Get a Ambulance by ID',
            validate: {
                params: Joi.object({
                    ambulanceId: Joi.string().required().description('ID of the Ambulance to fetch')
                })
            }
        },
        handler: async (request, h) => {
            const ambulanceId = request.params.ambulanceId;
            try {
                const ambulanceDetails = await ambulanceStore.getAmbulances({ id: ambulanceId });
                if (!isEmpty(ambulanceDetails)) {
                    return h.response({
                        statusCode: 200,
                        status: true,
                        message: `Successfully fetched details by ${ambulanceId}`,
                        data: ambulanceDetails
                    });
                } else {
                    return h.response({
                        statusCode: 404,
                        status: false,
                        message: `Ambulance with ID ${ambulanceId} not found`,
                        data: null
                    });
                }
            } catch (error) {
                console.error(error);
                return h.response({
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while fetching Ambulance data.',
                    data: null
                })
            }
        },
    },
     //Get By isActive status
     {
        method: 'GET',
        path: '/ambulance/getByStatus/{isActive}',
        options: {
            auth: false, // Apply JWT authentication to this route
            tags: ['api'],
            description: 'Get a Ambulance by isActive status',
            validate: {
                params: Joi.object({
                    isActive: Joi.boolean().required().description('ambulance data by isActive status')
                })
            }
        },
        handler: async (request, h) => {
            const isActive = request.params.isActive;
            try {
                const ambulanceDetails = await ambulanceStore.getAmbulances({ isActive: isActive });
                if (!isEmpty(ambulanceDetails)) {
                    return h.response({
                        statusCode: 200,
                        status: true,
                        message: `Successfully fetched ambulance details by isActive: ${isActive}`,
                        data: ambulanceDetails
                    });
                } else {
                    return h.response({
                        statusCode: 404,
                        status: false,
                        message: `Ambulance with isActive :${isActive} not foundd`,
                        data: null
                    });
                }
            } catch (error) {
                console.error(error);
                return h.response({
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while fetching Ambulance data.',
                    data: null
                })
            }
        },
    },
    //patch - Ambulance checking relations by id
    {
        method: 'GET',
        path: '/ambulance/checking/relations/{ambulanceId}',
        options: {
            auth: false, // Apply JWT authentication to this route
            tags: ['api'],
            description: 'Remove a Ambulance by ID',
            validate: {
                params: Joi.object({
                    ambulanceId: Joi.string().required().description('ID of the Ambulance to check relations')
                })
            }
        },
        handler: async (request, h) => {
            const ambulanceId = request.params.ambulanceId;
            let savedContent = '';
            let existingDriverData;
            const modifiedAt = new Date().toISOString();
            const modifiedBy = 'ADMIN';

            try {

                let data = {
                    id: ambulanceId
                }
                savedContent = await ambulanceStore.getAmbulances(data);
                console.log("savedContent", savedContent);

                if (isEmpty(savedContent)) {
                    return h.response({
                        status: false,
                        statusCode: 404,
                        error: 'Not Find Hospital',
                        message: `Please provide valid Hospital Id ${hospitalUserId}`,
                        data: null
                    })
                }

                if (savedContent == '') {
                    return h.response({
                        status: false,
                        statusCode: 404,
                        error: 'Not Find Ambulance',
                        message: `Please provide valid Ambulance ${ambulanceId} Id`,
                        data: null
                    })
                }

                const existingDriver = await driverStore.getDriverbyAmbulanceIdData(ambulanceId);
                if (isEmpty(existingDriver)) {
                    existingDriverData = null;
                } else {
                    existingDriverData = existingDriver;
                }

                response = {
                    ambulanceData: savedContent[0],
                    existingDriverData: existingDriverData
                }
                return h.response({
                    statusCode: 200,
                    status: true,
                    message: `Relations of ambulance`,
                    data: response
                })


            } catch (error) {
                return h.response({
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while Checking relations of ambulance.',
                    data: null
                })
            }
        },

    },
    //patch - Ambulance remove by id
    {
        method: 'PATCH',
        path: '/ambulance/remove/{ambulanceId}',
        options: {
            auth: false, // Apply JWT authentication to this route
            tags: ['api'],
            description: 'Remove a Ambulance by ID',
            validate: {
                params: Joi.object({
                    ambulanceId: Joi.string().required().description('ID of the Ambulance to delete')
                })
            }
        },
        handler: async (request, h) => {
            const ambulanceId = request.params.ambulanceId;
            let response, hospitalId;
            const modifiedAt = new Date().toISOString();
            const modifiedBy = 'ADMIN';

            try {

                let data = {
                    id: ambulanceId
                }

                const savedContent = await ambulanceStore.getAmbulances(data);
                // console.log("savedContent", savedContent);

                if (isEmpty(savedContent)) {
                    return h.response({
                        status: false,
                        statusCode: 404,
                        error: 'Not Find Ambulance',
                        message: `Please provide valid ${ambulanceId} Id`,
                        data: null
                    })
                }

                const existingDriver = await driverStore.getDriverbyAmbulanceIdData(ambulanceId);
                // console.log("existingDriver", existingDriver);
                if (existingDriver.length != 0) {

                    // Perform the update
                    for (let i = 0; i < existingDriver.length; i++) {
                        const updatedDriver = await driverStore.updateDriverById({
                            isActive: false, modifiedAt, modifiedBy
                        }, existingDriver[i].id);
                    }

                    // Perform the update
                    const updatedAmbulance = await ambulanceStore.updateAmbulanceById({
                        isActive: false, modifiedAt, modifiedBy
                    }, ambulanceId);

                    return h.response({
                        status: false,
                        statusCode: 200,
                        error: `Ambulance Id ${ambulanceId} Removed!`,
                        message: `Ambulance Id ${ambulanceId} Removed! `,
                        data: null
                    })
                }
                // console.log(existingDriver.length);
                if (existingDriver.length == 0) {
                    // Perform the update
                    const updatedAmbulance = await ambulanceStore.updateAmbulanceById({
                        isActive: false, modifiedAt, modifiedBy
                    }, ambulanceId);
                    // console.log("updatedAmbulance", updatedAmbulance);
                    return h.response({
                        statusCode: 200,
                        status: true,
                        message: `Ambulance Id ${ambulanceId} Removed!`,
                        data: updatedAmbulance
                    })
                }

            } catch (error) {
                return h.response({
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while deleting the ambulance.',
                    data: null
                })
            }
        },

    }
];







