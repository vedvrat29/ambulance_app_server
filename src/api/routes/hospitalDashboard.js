const Joi = require('joi');
const hospitalStore = require('../stores/hospitalStore');
const ridesStore = require('../stores/ridesStore');
const ambulanceStore = require('../stores/ambulanceStore');
const driverStore = require('../stores/driverStore');
const db = require('../../core/db');


module.exports = [

    // GET Dashboard status - 
    {
        method: 'GET',
        path: '/admin/hospital/dashboard/status/{hospitalId}',
        options: {
            auth: false, // Apply JWT authentication to this route
            tags: ['api'],
            description: 'Get a Hospital by ID',
            validate: {
                params: Joi.object({
                    hospitalId: Joi.string().required().description('ID of the Hospital to fetch')
                })
            }
        },
        handler: async (request, handler) => {
            const hospitalId = request.params.hospitalId;
            let getAllAmbulanceRidesData = [];
            try {

                let data = {
                    id: hospitalId
                }

                const hospitalDetails = await hospitalStore.getHospital(data);
                //getAllHospitalAmbulanceCount - ride
                let getAllHospitalAmbulanceCount = await ambulanceStore.getAllHospitalAmbulanceCount(hospitalId);
                //getAllHospitalDriverCount- ride
                let getAllHospitalDriverCount = await driverStore.getAllHospitalDriverCount(hospitalId);
                //getAllHospitalRidesCount- ride
                let getAllHospitalRidesCount = await ridesStore.getAllHospitalRidesCount(hospitalId);
                getAllAmbulanceRidesData = await ambulanceStore.getAllHospitalAmbulanceData(hospitalId);


                let ambulanceTypeCounts = {
                    "BASIC": 0,
                    "ADVANCE": 0,
                    "MATERNITY": 0
                }

                for (let i = 0; i < getAllAmbulanceRidesData.length; i++) {

                    const ride = getAllAmbulanceRidesData[i];
                    if (ride.hospitalId == hospitalId) {
                        ambulanceTypeCounts[ride.type]++; // Increment the count for the corresponding type
                    }
                }
              

                let response = {
                    totalHospitalRidesCount: +getAllHospitalRidesCount[0].count,
                    totalHospitalAmbulanceCount: +getAllHospitalAmbulanceCount[0].count,
                    totalHospitalDriverCount: +getAllHospitalDriverCount[0].count,
                    totalRidesByAmbulanceType: ambulanceTypeCounts,
                }
                // console.log("response", response);
                return handler.response({
                    status: true,
                    statusCode: 200,
                    error: '',
                    message: 'Admin hospital dashboard status',
                    data: response
                })

            } catch (error) {
                console.error("Error fetching hospital data:", error);
                return {
                    status: false,
                    statusCode: 500,
                    error: 'Internal Server Error',
                    message: 'An error occurred while fetching hospital dashboard data.',
                    data: null
                };
            }

        },
    },
];