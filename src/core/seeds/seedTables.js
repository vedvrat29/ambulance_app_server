const Users = require('../seeddata/Users');
const UserValidation = require('../seeddata/UsersValidation');
const GeneralParams = require('../seeddata/GeneralParams');
const Address = require('../seeddata/Address');
const Hospital = require('../seeddata/Hospital');
const Ambulance = require('../seeddata/Ambulance');
const Driver = require('../seeddata/Driver');
const Rides = require('../seeddata/Rides');

exports.seed = function (knex) {
    return Promise.all([
        // Inserts seed entries
        knex('Users').insert(Users),
        knex('UserValidation').insert(UserValidation),
        knex('GeneralParams').insert(GeneralParams),
        knex('Address').insert(Address),
        knex('Hospital').insert(Hospital),
        knex('Ambulance').insert(Ambulance),
        knex('Driver').insert(Driver),
        knex('Rides').insert(Rides)

    ])
}