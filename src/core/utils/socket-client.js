const io = require('socket.io-client');

// Connect to the Socket.io server
const socket = io('http://localhost:3000'); // Replace with your server's address
let roomName = "323e4567-e89b-12d3-a456-426614174002_a23e4567-e89b-12d3-a456-426614174600";
// let roomName = "323e4567-e89b-12d3-a456-426614174002_a23e4567-e89b-12d3-a456-426614174600";
// Handle connection events
socket.on('/connection', () => {
  console.log('Connected to the Socket.io server');
});

socket.on('receivedMessage', ({ sender, message }) => {
  console.log(`Message received from ${sender}: ${message}`);
});

socket.on('disconnect', () => {
  console.log('Disconnected from the Socket.io server');
});

socket.on('sendToRoom', ({ room, message }) => {
  console.log('client message...', message)
  // Emit the message to all clients in the specified room
  io.to(room).emit('receivedMessage', { sender: socket.id, message });
});

socket.on(roomName, ({ room, message }) => {
  console.log(`connect room`,room);
  console.log(message);

  socket.emit(roomName,{ room: room, message: message });
  // io.to(room).emit('receivedMessage', { room, message });
   // This will log the confirmation message from the server
});


// Emit a custom event to the server with room and message properties
socket.emit('sendToRoom', { room: 'testroom', message: 'Hello, Server!' });
socket.emit('receivedMessage', { room: 'testroom', message: 'Hello, Server!' });
// socket.emit('roomCreated', { message: 'Hello, Server!' });
socket.emit(roomName,{ room: 'roomName', message: 'Hello, Server!' });


// Handle errors
socket.on('error', (error) => {
  console.error('Socket.io error:', error);
});