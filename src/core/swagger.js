const Inert = require('@hapi/inert');
const Vision = require('@hapi/vision');
const HapiSwagger = require('hapi-swagger');
const Pack = require('./package');

const configureSwagger = async (server) => {
  await server.register([
    Inert,
    Vision,
    {
      plugin: HapiSwagger,
      options: {
        info: {
          title: 'Your API Documentation',
          version: Pack.version,
        },
      },
    },
  ]);
};

module.exports = configureSwagger;
