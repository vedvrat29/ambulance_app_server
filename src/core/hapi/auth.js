const aguid = require('aguid');
const jwt = require('jwt-simple');
// const config = require('../../../config');

function generateToken (user, exp) {
    const sid = aguid()
      exp = (Math.floor(new Date().getTime()) + exp) / 1000
  
    // generate a new jwt token expiring in 24 hours
    return {sid,
      token: jwt.encode({
        id: user.id,
        sid: sid,
        exp: exp || Math.floor(new Date().getTime() / 1000) + 7 * 24 * 60 * 60,
        // put the users authentication roles inside the jwt token
        scope: user.scope
      }, 'q5w4e*/s1^3d1QSDcz21')}
  }
  
function generateUserToken (user, exp) {
    let jwt = generateToken(user, exp)
    return jwt.token
  }
  

  module.exports = {
    generateUserToken,
  };