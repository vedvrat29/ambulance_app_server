exports.up = function (knex) {
    return Promise.all([
      knex.schema.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp"'),
  
      knex.schema.createTable('Users', function (table) {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.string('email').unique()
        table.string('phoneNumber').unique()
        table.string('fullName')
        table.string('password')
        table.json('scope')
        table.text('authToken')
        table.text('firebaseToken')
        table.text('fcmToken')
        table.string('dob')
        table.string('gender')
        table.string('signupType').notNullable()
        table.boolean('approved').default(false)
        table.boolean('disabled').default(false)
        table.timestamp('createdAt').notNullable().defaultTo(knex.fn.now())
        table.timestamp('modifiedAt')
        table.string('createdBy')
        table.string('modifiedBy')
      }),
      knex.schema.createTable('UserValidation', function (table) {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.uuid('userId').notNullable().references('Users.id').unique()
        table.boolean('isCurrentSession').default(false)
        table.string('emailValidationKey')
        table.boolean('isPhoneValid').default(false)
        table.boolean('isEmailValid').default(false)
        table.boolean('isAcceptTerms').default(false)
        table.boolean('isAcceptPrivacy').default(false)
        table.boolean('isDocSubmitted').default(false)
        table.string('otp')
        table.string('otpExpTime')
        table.timestamp('createdAt').notNullable().defaultTo(knex.fn.now())
        table.timestamp('modifiedAt')
        table.string('createdBy')
        table.string('modifiedBy')
      }),
      knex.schema.createTable('GeneralParams', function (table) {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.string('key').unique().notNullable()
        table.string('value').notNullable()
        table.string('description')
        table.string('category').notNullable()
        table.string('createdBy')
        table.timestamp('createdAt').notNullable().defaultTo(knex.fn.now())
        table.string('modifiedBy')
        table.timestamp('modifiedAt')
      }),
      knex.schema.createTable('Address', function (table) {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.uuid('userId').notNullable().references('Users.id')
        table.string('fullname')
        table.string('phoneNumber')
        table.string('addressLine1').notNullable()
        table.string('addressLine2')
        table.string('landmark')
        table.string('city').notNullable()
        table.string('state').notNullable()
        table.string('postalCode').notNullable()
        table.boolean('default').default(true)
        table.boolean('isActive').default(true)
        table.string('relation').notNullable()
        table.timestamp('createdAt').notNullable().defaultTo(knex.fn.now())
        table.timestamp('modifiedAt')
        table.string('createdBy')
        table.string('modifiedBy')
      }),
      knex.schema.createTable('Hospital', function (table) {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.uuid('userId').notNullable().references('Users.id')
        table.string('type')
        table.string('name')
        table.string('doctorName')
        table.string('doctorPhoneNumber')
        table.string('addressLine1').notNullable()
        table.string('addressLine2')
        table.string('landmark')
        table.string('city').notNullable()
        table.string('state').notNullable()
        table.string('postalCode').notNullable()
        table.double('latitude', 8, 6)
        table.double('longitude', 8, 6)
        table.boolean('isActive').default(true)
        table.timestamp('createdAt').notNullable().defaultTo(knex.fn.now())
        table.timestamp('modifiedAt')
        table.string('createdBy')
        table.string('modifiedBy')
      }),
      knex.schema.createTable('Ambulance', function (table) {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.uuid('hospitalId').notNullable().references('Hospital.id')
        table.string('type').notNullable()
        table.string('numberPlate').unique().notNullable()
        table.string('status').notNullable()
        table.boolean('isActive').default(true)
        table.timestamp('createdAt').notNullable().defaultTo(knex.fn.now())
        table.timestamp('modifiedAt')
        table.string('createdBy')
        table.string('modifiedBy')
      }),
      knex.schema.createTable('Driver', function (table) {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.uuid('userId').notNullable().references('Users.id')
        table.uuid('hospitalId').notNullable().references('Hospital.id')
        table.uuid('ambulanceId').notNullable().references('Ambulance.id')
        table.string('licenseNumber').notNullable()
        table.string('aadharCardNumber').notNullable()
        table.json('documentDetails')
        table.boolean('isActive').default(true)
        table.string('createdBy')
        table.timestamp('createdAt').notNullable().defaultTo(knex.fn.now())
        table.string('modifiedBy')
        table.timestamp('modifiedAt')
      }),
      knex.schema.createTable('Rides', function (table) {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.uuid('userId').notNullable().references('Users.id')
        table.uuid('ambulanceId').references('Ambulance.id')
        table.uuid('driverId').references('Driver.id')
        table.uuid('hospitalId').references('Hospital.id')
        table.string('bookingId').notNullable()
        table.text('pickupPoint')
        table.text('dropoffPoint')
        table.string('status')
        table.json('ridesSummary')
        table.json('locationSummary')
        table.string('createdBy')
        table.timestamp('createdAt').notNullable().defaultTo(knex.fn.now())
        table.string('modifiedBy')
        table.timestamp('modifiedAt')
      }),
      knex.schema.createTable('Transcations', function (table) {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.string('bookingId').notNullable()
        table.string('paymentId')
        table.string('calculatedPrice')
        table.string('discountedPrice')
        table.string('paymentType')
        table.string('paidAmount')
        table.string('refundAmount')
        table.string('refundType')
        table.string('status')
        table.string('createdBy')
        table.timestamp('createdAt').notNullable().defaultTo(knex.fn.now())
        table.string('modifiedBy')
        table.timestamp('modifiedAt')
      })
    ])
  }
  exports.down = function (knex) {
    return Promise.all([
  
      knex.raw('DROP TABLE IF EXISTS public."Users" CASCADE'),
      knex.raw('DROP TABLE IF EXISTS public."UserValidation" CASCADE'),
      knex.raw('DROP TABLE IF EXISTS public."GeneralParams" CASCADE'),
      knex.raw('DROP TABLE IF EXISTS public."Address" CASCADE'),
      knex.raw('DROP TABLE IF EXISTS public."Hospital" CASCADE'),
      knex.raw('DROP TABLE IF EXISTS public."Ambulance" CASCADE'),
      knex.raw('DROP TABLE IF EXISTS public."Driver" CASCADE'),
      knex.raw('DROP TABLE IF EXISTS public."Rides" CASCADE'),
      knex.raw('DROP TABLE IF EXISTS public."Transcations" CASCADE'),
    ])
  }