exports.up = function (knex) {
  /**
   * For any new tables created after the last update of this file, they need to be added here.
   */
  return Promise.all([

    knex.raw('DROP TABLE IF EXISTS public."Users" CASCADE'),
    knex.raw('DROP TABLE IF EXISTS public."UserValidation" CASCADE'),
    knex.raw('DROP TABLE IF EXISTS public."GeneralParams" CASCADE'),
    knex.raw('DROP TABLE IF EXISTS public."Address" CASCADE'),
    knex.raw('DROP TABLE IF EXISTS public."Hospital" CASCADE'),
    knex.raw('DROP TABLE IF EXISTS public."Ambulance" CASCADE'),
    knex.raw('DROP TABLE IF EXISTS public."Driver" CASCADE'),
    knex.raw('DROP TABLE IF EXISTS public."Rides" CASCADE'),
    knex.raw('DROP TABLE IF EXISTS public."Transcations" CASCADE'),
  ])
}

exports.down = function (knex) {
  return Promise.all([

  ])
}
