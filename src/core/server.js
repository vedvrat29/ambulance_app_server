const Hapi = require('@hapi/hapi');

const createServer = async (port, host, corsOrigin) => {
  const server = Hapi.server({
    port: port || 8080,
    host: host || '0.0.0.0',
    routes: {
      cors: {
        origin: corsOrigin || ['*'],
        credentials: true,
      },
    },
  });

  return server;
};

module.exports = createServer;
