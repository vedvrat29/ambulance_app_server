module.exports=[
    {
        id: '923e4567-e89b-12d3-a456-426614174300', // Static UUID
        userId: '323e4567-e89b-12d3-a456-426614174002', // Static UUID referencing Users.id
        fullname: 'John Doe',
        phoneNumber: '1234567890',
        addressLine1: '123 Main Street',
        addressLine2: 'Apt 4B',
        landmark: 'Near Park',
        city: 'City A',
        state: 'State X',
        postalCode: '12345',
        default: true,
        relation:'self',
        createdAt: new Date(),
        createdBy: 'Admin',
      },
      {
        id: 'a23e4567-e89b-12d3-a456-426614174301', // Static UUID
        userId: '323e4567-e89b-12d3-a456-426614174002', // Static UUID referencing Users.id
        fullname: 'Jane Smith',
        phoneNumber: '9876543210',
        addressLine1: '456 Oak Avenue',
        addressLine2: '',
        landmark: 'Corner Store',
        city: 'City B',
        state: 'State Y',
        postalCode: '54321',
        default: false,
        relation:'self',
        createdAt: new Date(),
        createdBy: 'Admin',
      },
      {
        id: 'b23e4567-e89b-12d3-a456-426614174302', // Static UUID
        userId: '323e4567-e89b-12d3-a456-426614174002', // Static UUID referencing Users.id
        fullname: 'Sam Johnson',
        phoneNumber: '5555555555',
        addressLine1: '789 Elm Street',
        addressLine2: 'Suite 101',
        landmark: 'Medical Center',
        city: 'City A',
        state: 'State X',
        postalCode: '98765',
        default: true,
        relation:'self',
        createdAt: new Date(),
        createdBy: 'Admin',
      },
      {
        id: 'c23e4567-e89b-12d3-a456-426614174303', // Static UUID
        userId: '423e4567-e89b-12d3-a456-426614174003', // Static UUID referencing Users.id
        fullname: 'Alice Lee',
        phoneNumber: '9999999999',
        addressLine1: '101 Pine Road',
        addressLine2: 'Unit A',
        landmark: 'Library',
        city: 'City C',
        state: 'State Z',
        postalCode: '67890',
        default: false,
        relation:'self',
        createdAt: new Date(),
        createdBy: 'Admin',
      },
      {
        id: 'c23e4567-e89b-12d3-a456-426614174305', // Static UUID
        userId: '423e4567-e89b-12d3-a456-426614174004', // Static UUID referencing Users.id
        fullname: 'Alice Lee2',
        phoneNumber: '9999999999',
        addressLine1: '102 Pine Road',
        addressLine2: 'Unit b',
        landmark: 'Library',
        city: 'City B',
        state: 'State X',
        postalCode: '67809',
        default: false,
        relation:'self',
        createdAt: new Date(),
        createdBy: 'Admin',
      },
]