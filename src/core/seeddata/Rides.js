module.exports=[
    {
        id: 'a23e4567-e89b-12d3-a456-426614174700', // Static UUID
        userId: '323e4567-e89b-12d3-a456-426614174002', // Static UUID referencing Users.id having scope customer
        ambulanceId: 'a23e4567-e89b-12d3-a456-426614174500', // Static UUID referencing Ambulance.id
        // driverId: 'a23e4567-e89b-12d3-a456-426614174600', // Static UUID referencing Driver.id
        hospitalId: 'a23e4567-e89b-12d3-a456-426614174400', // Static UUID referencing Hospital.id
        bookingId: '12ssdfsxc', // Static UUID for booking
        pickupPoint: 'Pickup Address 1',
        dropoffPoint: 'Drop-off Address 1',
        status: 'COMPLETED',
        ridesSummary: JSON.stringify([{logs:"driver accepted the ride",datetime:"TIMESTAMP"}]), // Example JSON data
        locationSummary: JSON.stringify([{driver:{lat:"12121212", lang:"12121212"},user:{lat:"12121212", lang:"12121212"},hospital:{lat:"12121212", lang:"12121212"}}]), // Example JSON data // Example JSON data
        createdBy: 'Admin',
        createdAt: new Date(),
      },
      {
        id: 'b23e4567-e89b-12d3-a456-426614174701', // Static UUID
        userId: '323e4567-e89b-12d3-a456-426614174002', // Static UUID referencing Users.id
        ambulanceId: 'b23e4567-e89b-12d3-a456-426614174501', // Static UUID referencing Ambulance.id
        // driverId: 'b23e4567-e89b-12d3-a456-426614174601', // Static UUID referencing Driver.id
        hospitalId: 'b23e4567-e89b-12d3-a456-426614174401', // Static UUID referencing Hospital.id
        bookingId: 'Edjsf23', // Static UUID for booking
        pickupPoint: 'Pickup Address 2',
        dropoffPoint: 'Drop-off Address 2',
        status: 'PENDING',
        ridesSummary: JSON.stringify([{logs:"driver accepted the ride",datetime:"TIMESTAMP"}]), // Example JSON data
        locationSummary: JSON.stringify([{driver:{lat:"12121212", lang:"12121212"},user:{lat:"12121212", lang:"12121212"},hospital:{lat:"12121212", lang:"12121212"}}]), // Example JSON data // Example JSON data
        createdBy: 'Admin',
        createdAt: new Date(),
      }
]