module.exports = [
    {
        id: 'a23e4567-e89b-12d3-a456-426614174400', // Static UUID
        userId: '223e4567-e89b-12d3-a456-426614174001', // Static UUID referencing Users.id
        type: 'PRIVATE',
        name: 'Sai Hospital',
        addressLine1: '123 Main Street',
        addressLine2: 'Apt 4B',
        landmark: 'Near Park',
        city: 'City A',
        state: 'State X',
        postalCode: '12345',
        latitude: 37.7749, // Example latitude value
        longitude: -122.4194, // Example longitude value
        isActive: true,
        createdAt: new Date(),
        createdBy: 'Admin',
      },
      {
        id: 'b23e4567-e89b-12d3-a456-426614174401', // Static UUID
        userId: '223e4567-e89b-12d3-a456-426614174011', // Static UUID referencing Users.id
        type: 'PRIVATE',
        name: 'Sasson Hospital',
        addressLine1: '456 Oak Avenue',
        addressLine2: '',
        landmark: 'Corner Store',
        city: 'City B',
        state: 'State Y',
        postalCode: '54321',
        latitude: 38.8895, // Example latitude value
        longitude: -77.0352, // Example longitude value
        isActive: false,
        createdAt: new Date(),
        createdBy: 'Admin',
      },
      {
        id: 'c23e4567-e89b-12d3-a456-426614174402', // Static UUID
        userId: '223e4567-e89b-12d3-a456-426614174012', // Static UUID referencing Users.id
        type: 'GOVERNMENT',
        name: 'Ruby Hospital',
        addressLine1: '789 Elm Street',
        addressLine2: 'Suite 101',
        landmark: 'Medical Center',
        city: 'City A',
        state: 'State X',
        postalCode: '98765',
        latitude: 40.7128, // Example latitude value
        longitude: -74.0060, // Example longitude value
        isActive: true,
        createdAt: new Date(),
        createdBy: 'Admin'
      },
]