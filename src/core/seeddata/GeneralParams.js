module.exports = [
    {
        id: '523e4567-e89b-12d3-a456-426614174200', // Static UUID
        key: 'param_key_1',
        value: 'value_1',
        description: 'Description 1',
        category: 'Category A',
        createdBy: 'Admin',
        createdAt: new Date()
      },
      {
        id: '623e4567-e89b-12d3-a456-426614174201', // Static UUID
        key: 'param_key_2',
        value: 'value_2',
        description: 'Description 2',
        category: 'Category B',
        createdBy: 'Admin',
        createdAt: new Date()
      },
      {
        id: '723e4567-e89b-12d3-a456-426614174202', // Static UUID
        key: 'param_key_3',
        value: 'value_3',
        description: 'Description 3',
        category: 'Category A',
        createdBy: 'Admin',
        createdAt: new Date()
      },
      {
        id: '823e4567-e89b-12d3-a456-426614174203', // Static UUID
        key: 'param_key_4',
        value: 'value_4',
        description: 'Description 4',
        category: 'Category C',
        createdBy: 'Admin',
        createdAt: new Date()
      },
]