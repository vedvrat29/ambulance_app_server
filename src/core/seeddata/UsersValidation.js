module.exports = [
{
    id: '123e4567-e89b-12d3-a456-426614174100', // Static UUID
    userId: '123e4567-e89b-12d3-a456-426614174000', // Static UUID referencing Users.id
    isCurrentSession: false,
    emailValidationKey: 'email_key_1',
    isPhoneValid: false,
    isEmailValid: false,
    isAcceptTerms: false,
    isAcceptPrivacy: false,
    isDocSubmitted: false,
    createdAt: new Date(),
    createdBy: 'Admin',
  },
  {
    id: '223e4567-e89b-12d3-a456-426614174101', // Static UUID
    userId: '223e4567-e89b-12d3-a456-426614174001', // Static UUID referencing Users.id
    isCurrentSession: true,
    emailValidationKey: 'email_key_2',
    isPhoneValid: true,
    isEmailValid: true,
    isAcceptTerms: true,
    isAcceptPrivacy: true,
    isDocSubmitted: true,
    createdAt: new Date(),
    createdBy: 'Admin',
  },
  {
    id: '323e4567-e89b-12d3-a456-426614174102', // Static UUID
    userId: '323e4567-e89b-12d3-a456-426614174002', // Static UUID referencing Users.id
    isCurrentSession: false,
    emailValidationKey: 'email_key_3',
    isPhoneValid: false,
    isEmailValid: false,
    isAcceptTerms: true,
    isAcceptPrivacy: false,
    isDocSubmitted: true,
    createdAt: new Date(),
    createdBy: 'Admin',
  },
  {
    id: '423e4567-e89b-12d3-a456-426614174103', // Static UUID
    userId: '423e4567-e89b-12d3-a456-426614174003', // Static UUID referencing Users.id
    isCurrentSession: true,
    emailValidationKey: 'email_key_4',
    isPhoneValid: true,
    isEmailValid: false,
    isAcceptTerms: true,
    isAcceptPrivacy: true,
    isDocSubmitted: false,
    createdAt: new Date(),
    createdBy: 'Admin',
  },
  {
    id: '423e4567-e89b-12d3-a456-426614174104', // Static UUID
    userId: '423e4567-e89b-12d3-a456-426614174004', // Static UUID referencing Users.id
    isCurrentSession: true,
    emailValidationKey: 'email_key_4',
    isPhoneValid: true,
    isEmailValid: false,
    isAcceptTerms: true,
    isAcceptPrivacy: true,
    isDocSubmitted: false,
    createdAt: new Date(),
    createdBy: 'Admin',
  }
];