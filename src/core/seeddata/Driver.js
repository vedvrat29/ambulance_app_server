module.exports=[
    {
        id: 'a23e4567-e89b-12d3-a456-426614174600', // Static UUID
        userId: '423e4567-e89b-12d3-a456-426614174003', // Static UUID referencing Users.id
        hospitalId: 'a23e4567-e89b-12d3-a456-426614174400', // Static UUID referencing Hospital.id
        ambulanceId: 'a23e4567-e89b-12d3-a456-426614174500', // Static UUID referencing Ambulance.id
        licenseNumber: 'DL12345',
        aadharCardNumber: 'AADHAR12345',
        documentDetails: JSON.stringify([{docName:"license",path:"imagepath.jpg"}]), // Example JSON data
        isActive: true,
        createdBy: 'ADMIN',
        createdAt: new Date(),
        modifiedBy: 'ADMIN',
        modifiedAt: new Date(),
      },
      {
        id: 'b23e4567-e89b-12d3-a456-426614174601', // Static UUID
        userId: '423e4567-e89b-12d3-a456-426614174004', // Static UUID referencing Users.id
        hospitalId: 'b23e4567-e89b-12d3-a456-426614174401', // Static UUID referencing Hospital.id
        ambulanceId: 'b23e4567-e89b-12d3-a456-426614174501', // Static UUID referencing Ambulance.id
        licenseNumber: 'DL54321',
        aadharCardNumber: 'AADHAR54321',
        documentDetails: JSON.stringify([{docName:"license2",path:"imagepath2.jpg"}]), // Example JSON data
        isActive: true,
        createdBy: 'ADMIN',
        createdAt: new Date(),
        modifiedBy: 'ADMIN',
        modifiedAt: new Date(),
      },
      
]