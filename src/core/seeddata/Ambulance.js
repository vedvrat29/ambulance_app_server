module.exports = [
    {
        id: 'a23e4567-e89b-12d3-a456-426614174500', // Static UUID
        hospitalId: 'a23e4567-e89b-12d3-a456-426614174400', // Static UUID referencing Hospital.id
        type: 'BASIC',
        numberPlate: 'AB-123-XY',
        status: 'AVAILABLE',
        isActive: true,
        createdAt: new Date(),
        createdBy: 'Admin',
        
      },
      {
        id: 'b23e4567-e89b-12d3-a456-426614174501', // Static UUID
        hospitalId: 'b23e4567-e89b-12d3-a456-426614174401', // Static UUID referencing Hospital.id
        type: 'ADVANCE',
        numberPlate: 'CD-456-ZW',
        status: 'AVAILABLE',
        isActive: true,
        createdAt: new Date(),
        createdBy: 'Admin',
        
      }
]