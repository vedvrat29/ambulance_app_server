const socketIo = require('socket.io');

const configureSocketIO = (server, socketIOPort) => {
  const io = socketIo(server.listener);
  io.listen(socketIOPort || 3001);
  console.log('Socket.io server listening on port', socketIOPort || 3001);
};

module.exports = configureSocketIO;
