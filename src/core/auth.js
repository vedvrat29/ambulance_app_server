const Jwt = require('hapi-auth-jwt2');

const configureAuth = (server, jwtSecret) => {
  server.register(Jwt);
  
  server.auth.strategy('jwt', 'jwt', {
    key: jwtSecret || 'your-secret-key',
    verifyOptions: { algorithms: ['HS256'] },
    validate: async (decoded, request) => {
      // Add your validation logic here
      return { isValid: true };
    },
  });

  server.auth.default('jwt');
};

module.exports = configureAuth;
