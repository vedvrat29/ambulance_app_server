const bcrypt = require('bcrypt');
const userstore = require('../../api/stores/usersStore');
const { generateToken } = require('../token');

async function Signup({ fullName, email, password, phoneNumber, role, signupType,dob,gender,createdBy }, userRole) {
  const salted = bcrypt.hashSync(password, bcrypt.genSaltSync(10), null);

  let userId, scope;
  try {
    userId = (await userstore.create({ fullName, email, phoneNumber, signupType, password: salted,scope: JSON.stringify([userRole]),dob,gender,createdBy  }))[0];
    scope = [userRole];
  } catch (e) {
    if (e.constraint === 'users_username_unique') {
      let existingCustomer = await userstore.getByUserEmail(email) || userstore.getUserByPhoneNumber(phoneNumber);
      if (existingCustomer.scope.indexOf(userRole) === -1) {
        await userstore.addcustomer(existingCustomer.scope, userRole);
        userId = existingCustomer.id;
        scope = existingCustomer.scope;
        scope.push(userRole);
      } else {
        throw e;role
      }
    } else {
      console.log(e); // Log the error
      throw new Error('Unknown Error');
    }
  }

  let user = { data: userId, scope }
  let authToken = await generateToken(user)
  await userstore.updateUserToken(user.data.id, authToken)
  return { authToken, user }
}


async function signupHospital({ fullName, email, password, phoneNumber, role, signupType,createdBy }, userRole) {
  const salted = bcrypt.hashSync(password, bcrypt.genSaltSync(10), null);

  let userId, scope;
  try {
    userId = (await userstore.create({ fullName, email, phoneNumber, signupType, password: salted,scope: JSON.stringify([userRole]),createdBy  }))[0];
    scope = [userRole];
  } catch (e) {
    console.log(e);
    if (e.constraint === 'users_username_unique') {
      let existingCustomer = await userstore.getByUserEmail(email) || userstore.getUserByPhoneNumber(phoneNumber);
      if (existingCustomer.scope.indexOf(userRole) === -1) {
        await userstore.addcustomer(existingCustomer.scope, userRole);
        userId = existingCustomer.id;
        scope = existingCustomer.scope;
        scope.push(userRole);
      } else {
        throw e;role
      }
    } else {
      console.log(e); // Log the error
      throw new Error('Unknown Error');
    }
  }

  let user = { data: userId, scope }
  let authToken = await generateToken(user)
  await userstore.updateUserToken(user.data.id, authToken)
  return { authToken, user }
}


async function signupCustomer({ fullName, phoneNumber, signupType,createdBy }, userRole) {
  const salted = bcrypt.hashSync(password, bcrypt.genSaltSync(10), null);

  let userId, scope;
  try {
    userId = (await userstore.create({ fullName, phoneNumber, signupType,scope: JSON.stringify([userRole]),createdBy }))[0];
    scope = [userRole];
  } catch (e) {
    console.log(e);
    if (e.constraint === 'users_username_unique') {
      let existingCustomer = userstore.getUserByPhoneNumber(phoneNumber);
      if (existingCustomer.scope.indexOf(userRole) === -1) {
        await userstore.addcustomer(existingCustomer.scope, userRole);
        userId = existingCustomer.id;
        scope = existingCustomer.scope;
        scope.push(userRole);
      } else {
        throw e;role
      }
    } else {
      console.log(e); // Log the error
      throw new Error('Unknown Error');
    }
  }

  let user = { data: userId, scope }
  let authToken = await generateToken(user)
  await userstore.updateUserToken(user.data.id, authToken)
  return { authToken, user }
}


async function SignupOtp({ fullName, phoneNumber,fcmToken}, userRole) {
  // const salted = bcrypt.hashSync(password, bcrypt.genSaltSync(10), null);
  let userId, scope;

  try {
    userId = (await userstore.create({ fullName, phoneNumber, scope: JSON.stringify([userRole]), signupType:'NA',fcmToken  }))[0];
    scope = [userRole];
  } catch (e) {
    if (e.constraint === 'users_username_unique') {
      let existingCustomer = await userstore.getByUserEmail(email) || userstore.getUserByPhoneNumber(phoneNumber);
      if (existingCustomer.scope.indexOf(userRole) === -1) {
        await userstore.addcustomer(existingCustomer.scope, userRole);
        userId = existingCustomer.id;
        scope = existingCustomer.scope;
        scope.push(userRole);
      } else {
        throw e;
      }
    } else {
      console.log(e); // Log the error
      throw new Error('Unknown Error');
    }
  }

  let user = { data: userId, scope }
  let authToken = await generateToken(user)
  await userstore.updateUserToken(user.data.id, authToken)
  return { authToken, user }
}

module.exports = {
  Signup,
  signupHospital,
  signupCustomer,
  SignupOtp
};
