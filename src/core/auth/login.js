const usersStore = require("../../api/stores/usersStore");


async function userPhonelogin({ phoneNumber, otp, rememberMe }, role) {
    // randomize the result to prevent timing attacks

    await setTimeoutAsync(Math.floor(Math.random() * (500 + 1)))
    // Lookup existing user
    // email = email.toLowerCase()
    const user = await usersStore.getUserByPhoneNumber(phoneNumber)
    logger.info('Fetching User Info for Phone Login -', JSON.stringify(user))


    //update vendor role
    if (user.scope[0] === 'driver' && role === 'Customer' && user.scope.length === 1) {
        await userstore.updateScope(user.scope, role, user.id)
        //update "customer scope"
    }
    if (user.scope[0] === 'investor' && role === 'Admin') {
        logger.info("customer cannot login as a vendor.")
        throw new UserNotFoundError()
    }
    // user not found
    if (isEmpty(user)) {
        throw new UserNotFoundError()
    } else if (user.scope.indexOf(role) === -1) {
        throw new UserRoleInvalidError()
    }
    let { authToken, updatedUser } = await authenticatePhoneUser(user, otp, rememberMe)
    return {
        'authToken': authToken,
        'user': updatedUser[0],
    }
}


module.exports = {
    userPhonelogin,
};