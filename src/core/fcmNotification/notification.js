const admin = require('firebase-admin');
const serviceAccount = require('../../core/fcmNotification/jeevan-driver-app.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});

async function sendFcmNotification(customData,fcmToken,title,body){  
  
    const message = {
        data: {
          title: title,
          body: body,
          customData: JSON.stringify(customData),
        },
        tokens: fcmToken,
      };
      
      admin.messaging().sendMulticast(message)
        .then((response) => {
          console.log('Notification sent successfully:', response);
        })
        .catch((error) => {
          console.error('Error sending notification:', error);
        });
}

module.exports = {
    sendFcmNotification
};

