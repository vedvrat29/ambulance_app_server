const admin = require('firebase-admin');
const serviceAccountCustomer = require('../../core/fcmNotification/jeevan-customer-app.json');
const serviceAccountAnother = require('../../core/fcmNotification/jeevan-driver-app.json');

// Initialize the first app with a unique name
const customer = admin.initializeApp({
  credential: admin.credential.cert(serviceAccountCustomer),
}, 'customer');

// Initialize the second app with a different unique name
const driver = admin.initializeApp({
  credential: admin.credential.cert(serviceAccountAnother),
}, 'driver');

async function sendFcmNotificationToCustomer(customData, fcmToken, title, body,appName) {  
  let app;

  // Determine which Firebase app to use based on the 'appName' parameter
   if (appName === 'customer') {
    console.log(`notification to ${appName}.......`);
    app = customer;
} else if (appName === 'driver') {
  console.log(`notification to ${appName}.......`);

    app = driver;
} else {
    console.error('Invalid app name:', appName);
    return;
}  
  
  const message = {
        data: {
          title: title,
          body: body,
          customData: JSON.stringify(customData),
        },
        tokens: fcmToken,
      };
      
      app.messaging().sendMulticast(message) // Use app1 or app2 based on which app you want to send the notification.
        .then((response) => {
          console.log('Notification sent successfully:', response);
        })
        .catch((error) => {
          console.error('Error sending notification:', error);
        });
}

module.exports = {
  sendFcmNotificationToCustomer
};
