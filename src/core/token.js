const bcrypt = require('bcrypt');
const { generateUserToken } = require('../core/hapi/auth');

// generate token
function generateToken(user, rememberMe, exp) {
    const forever = 365 * 30 * 7 * 24 * 60 * 60 * 1000
    let cookieOptions = rememberMe ? { ttl: forever } : { ttl: exp }
    return generateUserToken(user, cookieOptions.ttl)
}

//  generate random token
function generateRandomToken(length) {
    // edit the token allowed characters
    var a = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'.split('')
    var b = []
    for (var i = 0; i < length; i++) {
        var j = (Math.random() * (a.length - 1)).toFixed(0)
        b[i] = a[j]
    }
    return b.join('')
}

// salt password
function saltedPassword(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null)
}

module.exports = {
    generateToken,
    generateRandomToken,
    saltedPassword
  };
