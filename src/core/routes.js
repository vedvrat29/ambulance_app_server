const fs = require('fs');
const path = require('path');

const registerRoutes = (server) => {
  const routeFiles = fs.readdirSync(path.join(__dirname, './src/api/routes'));
  for (const file of routeFiles) {
    if (file.endsWith('.js')) {
      const routesFromFile = require(`./src/api/routes/${file}`);
      for (const route of routesFromFile) {
        server.route(route);
      }
    }
  }
};

module.exports = registerRoutes;
