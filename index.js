const dotenv = require('dotenv');
const configureAuth = require('./src/core/auth');
const registerRoutes = require('./src/core/routes');
const configureSocketIO = require('./src/core/socket');
const configureSwagger = require('./src/core/swagger');


dotenv.config();

const { PORT, HOST, JWT_SECRET, SOCKET_IO_PORT } = process.env;

const init = async () => {
  const server = await createServer(PORT, HOST, ['*']);
  configureAuth(server, JWT_SECRET);
  await configureSwagger(server);
  registerRoutes(server);
  await server.start();
  configureSocketIO(server, SOCKET_IO_PORT);
  console.log('Hapi Server running on', server.info.uri);
};

init();
